import { AppPage } from './app.po';

describe('app App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Personal Health Record');

  });
  it('should login to admin', () => {
    page.navigateTo();
    page.setUserName('admin');
    page.setPassword('admin');
    page.getButtonLogin().click();
    expect(page.getMessage()).toEqual('Gagal Login');
  })
});
