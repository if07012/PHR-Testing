import { browser, element, by } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }
  getMessage() {
    return element(by.id('sign_id_header_message')).getText();
  }
  getParagraphText() {
    return element(by.id('sign_in_header')).getText();
  }
  getButtonLogin() {
    return element(by.id('sign_in_login'));
  }
  setUserName(val) {
    element(by.id('sign_in_username')).sendKeys(val);
  }
  setPassword(val) {
    element(by.id('sign_in_password')).sendKeys(val);
  }
}
