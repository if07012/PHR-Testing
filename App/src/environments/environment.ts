export const environment = {
  production: true,
  AUTHENTICATION_ENDPOINT:'http://localhost:3399/',
  SYSADMIN_ENDPOINT:'http://localhost:1749/',
  PATIENT_ENDPOINT:'http://localhost:3432/',
  MESSAGING_ENDPOINT:'http://localhost:3000/',
  envName:'dev',
  isTesting:true
};
