export const environment = {
  production: true,
  AUTHENTICATION_ENDPOINT:'http://auth.phr.com/',
  SYSADMIN_ENDPOINT:'http://sysadmin.phr.com/',
  PATIENT_ENDPOINT:'http://patient.phr.com/',
  MESSAGING_ENDPOINT:'http://messaging.phr.com/',
  envName:'prod',
  isTesting:false
};
