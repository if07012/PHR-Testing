export const environment = {
  production: true,
  AUTHENTICATION_ENDPOINT:'http://localhost:7000/',
  SYSADMIN_ENDPOINT:'http://localhost:7001/',
  PATIENT_ENDPOINT:'http://localhost:7002/',
  MESSAGING_ENDPOINT:'http://localhost:7008/',
  envName:'prod',
  isTesting:true
};
