export class InputTextItem {
    public data :any;
    public required:boolean;
    public hideTitle:boolean;

    public mask:string;
    public id:string;
    constructor(public title:string,public placeholder:string,public type:string){
        
    }
}