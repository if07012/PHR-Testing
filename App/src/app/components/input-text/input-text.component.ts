import { AuthenticationService } from './../../services/authentication';
import { InputTextItem } from './models/input.text.item';
import { Component, Input, ElementRef, ViewChild, AfterViewInit, Output, EventEmitter, DoCheck } from '@angular/core';
declare var $: any;
@Component({
  selector: 'phr-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements AfterViewInit {
  afterValue: any;
  textValue: string;
  result: Array<any>;

  value: any;
  @Input()
  validation: any;
  @Input()
  get val() {
    return this.value;
  }
  @Output()
  valChange = new EventEmitter();

  set val(data) {
    //recurive change
    if (this.value === data) return;
    if (this.declaration != null)
      if (this.declaration.type === "number") {
        if (this.declaration.data !== undefined) {
          if (+data < +this.declaration.data.min)
            data = +this.declaration.data.min;
          if (+data > +this.declaration.data.max)
            data = +this.declaration.data.max;
        }
      }
    this.value = data;
    if (data !== undefined && data !== null && this.declaration !== undefined) {
      if (this.selectEl !== undefined && this.declaration.data !== undefined) {
        $(this.selectEl.nativeElement).search('set value', data[this.declaration.data.fields.title]);
        $(this.selectEl.nativeElement).search('set result', data);
      } else if (this.dateEl != undefined) {
        $(this.dateEl.nativeElement).calendar('set date', data);
      }
    }
    if (data === undefined) {
      if (this.selectEl !== undefined) {
        $(this.selectEl.nativeElement).search('set value', '');
        $(this.selectEl.nativeElement).search('set result', undefined);
      }
    }
    this.valChange.emit(this.value);
  }
  @Output()
  selected: EventEmitter<any> = new EventEmitter<any>();
  ngDoCheck() {
    if (this.afterLoad !== this.declaration) {
      if (this.selectEl != undefined) {
        this.RescreateSelect();
      }

      else {
      }
      this.afterLoad = this.declaration;
    }
    if (this.validation !== undefined) {
    }

  }
  ngAfterViewInit(): void {
    this.afterLoad = this.declaration;
    let control = this;
    setTimeout(() => {
      if (this.dateEl != undefined) {
        var input = $(this.dateEl.nativeElement).find('input')[0];
        $(input).mask('00-00-0000');
        $(this.dateEl.nativeElement).calendar({
          type: 'date',
          monthFirst: false,
          onChange: function (date, text, model) {
            control.val = (date);
            control.selected.emit(control.val);
          }, formatter: {
            date: function (date, settings) {
              if (!date) {
                return '';
              }
              var day = date.getDate();
              var month = date.getMonth() + 1;
              if (month < 10)
                month = '0' + month;
              var year = date.getFullYear();
              if (parseInt(day) < 10)
                day = "0" + day;
              return day + '-' + month + "-" + year;
            }
          }
        });
        if (this.value != undefined) {
          $(this.dateEl.nativeElement).calendar('set date', this.value);
        }
      } else if (this.selectEl != undefined) {
        this.RescreateSelect();
        if (this.value !== undefined) {
          $(this.selectEl.nativeElement).search('set value', this.value[this.declaration.data.fields.title]);
          $(this.selectEl.nativeElement).search('set result', this.value);
        } else {
          $(this.selectEl.nativeElement).search('set value', '');
          $(this.selectEl.nativeElement).search('set result', undefined);
        }
      } else if (this.stringElem !== undefined) {
        if (this.declaration.mask !== undefined)
          $(this.stringElem.nativeElement).mask(this.declaration.mask);

      }
    }, 1000);

  }

  RescreateSelect(): void {
    let self = this.declaration;
    let control = this;
    let token = this.service.Token();
    if (this.declaration.data !== undefined) {
      if (this.declaration.data.source !== undefined) {
        $(this.selectEl.nativeElement).search(this.declaration.data);
      } else
        $(this.selectEl.nativeElement).search({
          onResultsClose: function (res) {
            setTimeout(function () {
              let result = $(control.selectEl.nativeElement).search('get value');
              control.val = $(control.selectEl.nativeElement).search('get result', result).data;
              control.selected.emit(control.val);
            }, 500)
          },
          apiSettings: {
            url: this.declaration.data.url + "/{query}",
            beforeXHR: function (xhr) {
              xhr.setRequestHeader('phr-token', token);
              return xhr;
            },
            'phr-token': token,
            onResponse: function (e) {
              var
                response = {
                  results: []
                };
              control.result = e;
              for (var i in e) {
                let model = {
                  title: e[i][self.data.fields.title],
                  description: 'asdasd',
                  data: e[i]
                };
                model[self.data.fields.title] = e[i][self.data.fields.title];
                response.results.push(model);
              }

              return response;
            },
          },
          fields: this.declaration.data.fields,
          minCharacters: 1
        });
    }
    this.val = this.value;
  }

  @ViewChild('dateElem') dateEl: ElementRef;
  @ViewChild('selectElem') selectEl: ElementRef;
  @ViewChild('stringElem') stringElem: ElementRef;
  @Input()
  declaration: InputTextItem;
  afterLoad: InputTextItem;

  @Input()
  data: any;
  constructor(private service: AuthenticationService) { }


}
