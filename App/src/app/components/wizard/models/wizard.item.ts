export class WizardItem {

    public cssClass: string;

    public id: string;

    public title: string;
    public description: string;
    public active: Boolean;
    public disable: Boolean;
    public completed: Boolean;
    constructor(
        id: string,
        title: string,
        description: string,

        active: Boolean,
        disable: Boolean
    ) {
        this.cssClass = "step";
        this.id = id;
        this.active = active;
        this.disable = disable;
        this.description = description;
        this.title = title;
        this.ReCreateCssClass();
    }

    ReCreateCssClass() {
        if (this.active == true)
            this.cssClass = "active step";
        if (this.disable == true)
            this.cssClass = "disabled step";
        if (this.completed == true)
            this.cssClass = "completed step";
    }
    Active() {
        this.active = true;
        this.disable = false;
        this.ReCreateCssClass();
    }

    Disabled() {
        this.active = false;
        this.disable = true;
        this.ReCreateCssClass();
    }
    Completed() {
        this.active = false;
        this.disable = false;
        this.completed = true;
        this.ReCreateCssClass();
    }
}

