import { WizardItem } from './models/wizard.item';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'phr-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit {

  @Input()
  margin: number;
  @Input()
  items: Array<WizardItem>;
  @Input()
  classDecalaration: string;
  constructor() { }

  ngOnInit() {
    this.classDecalaration = "ui five steps";
  }

}
