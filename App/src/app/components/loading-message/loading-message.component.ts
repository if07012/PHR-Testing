import { Component, OnInit, Input } from '@angular/core';
import { MessageType } from '../../models/messageType';

@Component({
  selector: 'phr-loading-message',
  templateUrl: './loading-message.component.html',
  styleUrls: ['./loading-message.component.css']
})
export class LoadingMessageComponent implements OnInit {
  @Input()
  message: MessageType;
  
  constructor() { }

  ngOnInit() {
  }

}
