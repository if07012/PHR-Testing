import { Component, OnInit, AfterViewInit, ElementRef, ViewChild, Input, EventEmitter, Output } from '@angular/core';
declare var $: any;
@Component({
  selector: 'phr-button-combo',
  templateUrl: './button-combo.component.html',
  styleUrls: ['./button-combo.component.scss']
})
export class ButtonComboComponent implements OnInit, AfterViewInit {
  titleLocal: string;
  _isLoading: boolean;
  list: Array<string>;

  @Input()
  display:string;
  @Input()
  set isLoading(val: boolean) {
    this._isLoading = val;
    if (this.selectEl !== undefined)
      $(this.selectEl.nativeElement)
        .dropdown({
          on: 'click'
        });
  }
  get isLoading() {
    return this._isLoading;
  }



  @Output()
  changeState: EventEmitter<string> = new EventEmitter<string>();
  @Input()
  set action(val: Array<string>) {
    this.list = val;
  }
  get action() {
    return this.list;
  }
  raiseState(val: string) {
    this.changeState.next(val);
  }
  @Input()
  set title(val: string) {
    this.titleLocal = val;
  }
  get title() {
    return this.title;
  }
  @ViewChild('dropdownElem') selectEl: ElementRef;
  ngAfterViewInit(): void {
    if (this.selectEl !== undefined)
      $(this.selectEl.nativeElement)
        .dropdown({
          on: 'click'
        });
  }

  constructor() { }

  ngOnInit() {
  }

}
