import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonComboComponent } from './button-combo.component';

describe('ButtonComboComponent', () => {
  let component: ButtonComboComponent;
  let fixture: ComponentFixture<ButtonComboComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ButtonComboComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonComboComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
