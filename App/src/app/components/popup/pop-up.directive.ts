import { Directive, ElementRef, Renderer, AnimationKeyframe, AnimationStyles, style, animate } from '@angular/core';
declare var $: any;
@Directive({
  selector: '[phrPopUp]'
})
export class PopUpDirective {

  constructor(el: ElementRef, renderer: Renderer) {
    $(el.nativeElement).each(function () {
      $(this)
        .popup({
          on: 'hover',
          variation: 'small inverted',
          exclusive: true,
          content: $(this).attr('title')
        });
    });
    
  }

}
