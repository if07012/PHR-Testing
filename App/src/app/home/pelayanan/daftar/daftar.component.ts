import { Component, OnInit, AfterViewInit } from '@angular/core';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { AppSettings } from 'app/helper/appsetting';
import { Router, Routes, ActivatedRoute } from '@angular/router';
import { PatientService } from 'app/services/patient.service';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { MessageType } from 'app/models/messageType';
import { PushNotificationService } from 'app/services/push-notification.service';
import { NewEcgFilter } from 'app/home/pelayanan/ecg/new/model/new.ecg.filter';
import { environment } from 'environments/environment';
import { CoreHttpService } from 'app/services/core-http.service';
declare var $: any;
@Component({
  selector: 'phr-daftar-pelayanan',
  templateUrl: './daftar.component.html',
  styleUrls: ['./daftar.component.scss']
})
export class DaftarPelayananComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    this.checkNewEcg();
  }
  items: Array<WizardItem>;
  message: MessageType;

  registrasiModel: RegistrasiPelayananPasienModel;
  registrationId: string;
  countNewEcg: number;
  isTesting: boolean;
  constructor(private coreHttp: CoreHttpService,private notification: PushNotificationService,
    private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    this.message = new MessageType();
    this.registrasiModel = new RegistrasiPelayananPasienModel();
    let self = this;
    this.route.params.subscribe(n => {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.registrationId = n.id;
      pasienService.getRegistrasiPelayanan(this.registrationId).subscribe(result => {
        this.registrasiModel = result.data;
        this.checkNewEcg();
        this.notification.receive.subscribe(result => {
          this.checkNewEcg();
        });
        console.log("Subscribe to id : " + result.data.pasien.id);
        this.notification.subscribe = result.data.pasien.id;
        this.message = new MessageType();
      });
    });
    this.isTesting = environment.isTesting;
  }
  backToDaftar(){
    this.router.navigate(["home", "daftar","registrasi-pelayanan"]);
  }
  sendEcg(){
    this.coreHttp.Get(environment.PATIENT_ENDPOINT + "Pasien/SendEcg/" + this.registrasiModel.pasien.id).subscribe(result => {
      
    });
  }
  checkNewEcg() {
    let filter = new NewEcgFilter();
    if (this.registrasiModel === undefined) return;
    if (this.registrasiModel.pasien === undefined) return;
    if (this.registrasiModel.pasien.id === undefined) return;
    filter.pasienId = this.registrasiModel.pasien.id;
    this.pasienService.countNewEcg(filter).subscribe(result => {
      this.countNewEcg = result.totalRecords;
    })
  }
  showDiagnosa() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'input', "diagnosa"]);
  }
  daftarDiagnosa() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'daftar', "diagnosa"]);
  }
  showCatatanKlinis() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'input', "catatan-klinis"]);
  }
  daftarCatatanKlinis() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'daftar', "catatan-klinis"]);
  }
  daftarEcg() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'daftar', "ecg"]);
  }
  newEcg() {
    this.router.navigate(["home", "pelayanan", this.registrationId, 'new', "ecg"]);
  }
  showLampiran(){
    this.router.navigate(["home", "pelayanan", this.registrationId, 'lampiran', "new"]);
  }
  daftarLampiran(){
    this.router.navigate(["home", "pelayanan", this.registrationId, 'lampiran', "daftar"]);
  }

  showObat(){
    this.router.navigate(["home", "pelayanan", this.registrationId, 'obat', "new"]);
  }
  daftarObat(){
    this.router.navigate(["home", "pelayanan", this.registrationId, 'obat', "daftar"]);
  }

  notFound() {

  }
  ngOnInit() {
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Disabled();
    wizard[1].Disabled();
    wizard[2].Active();
    this.items = wizard;
  }

}
