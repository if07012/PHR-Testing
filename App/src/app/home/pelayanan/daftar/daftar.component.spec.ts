import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarPelayananComponent } from './daftar.component';

describe('DaftarComponent', () => {
  let component: DaftarPelayananComponent;
  let fixture: ComponentFixture<DaftarPelayananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarPelayananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarPelayananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
