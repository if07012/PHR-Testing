import { Component, OnInit, AfterViewInit } from '@angular/core';
import { environment } from 'environments/environment';
import { AuthenticationService } from "app/services/authentication";
import { ActivatedRoute } from '@angular/router';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { RegistrasiPelayananLampiranModel } from "app/models/registrasi.pelayanan.lampiran";
import { MessageType } from 'app/models/messageType';
import { PatientService } from 'app/services/patient.service';
declare var $: any;
declare var Dropzone: any;
@Component({
  selector: 'phr-lampiran',
  templateUrl: './lampiran.component.html',
  styleUrls: ['./lampiran.component.css']
})
export class LampiranComponent implements OnInit, AfterViewInit {
  message: MessageType = new MessageType();
  jenisLampiranMetaData: InputTextItem;
  url: string;
  currentModel: RegistrasiPelayananLampiranModel = new RegistrasiPelayananLampiranModel();
  ngAfterViewInit(): void {
    debugger;
    Dropzone._autoDiscoverFunction();
    let self = this;
    $("div#dropzone").dropzone({
      url: this.url,
      headers: {
        'phr-token': this.service.Token()
      }
    });

  }


  constructor(private service: AuthenticationService, private route: ActivatedRoute, private patientService: PatientService) {
    route.parent.params.subscribe(n => {
      this.url = environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Lampiran/" + n.id;
    });

    this.jenisLampiranMetaData = new InputTextItem("Lampiran", "Isi dengan Jenis Lampiran", "select");
    this.jenisLampiranMetaData.required = true;
    this.jenisLampiranMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "JenisLampiran/Search",
      fields: { title: 'namaJenisLampiran', description: 'Lampiran' },
      searchFullText: true
    };
    let parent = this.route.parent;
    parent.params.subscribe(params => {
      this.patientService.getRegistrasiPelayanan(params.id).subscribe(result => {
        this.currentModel.registrasiPasien = result.data;
        this.currentModel.pasien = result.data.pasien;
      });
    })
  }

  ngOnInit() {
  }

  saveLampiran(): void {

    if (this.currentModel.IsValid()) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.patientService.saveLampiranPelayanan(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {

          }
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }

}
