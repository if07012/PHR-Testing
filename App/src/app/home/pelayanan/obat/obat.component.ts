import { Component, OnInit } from '@angular/core';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { MessageType } from 'app/models/messageType';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from 'app/services/patient.service';

import { AppSettings } from "app/helper/appsetting";
import { RegistrasiPelayananObatModel, RegistrasiPelayananObatDetailModel } from "app/models/registrasi.pelayanan.obat";
import { environment } from "environments/environment";
import { PushNotificationService } from 'app/services/push-notification.service';

@Component({
  selector: 'phr-obat',
  templateUrl: './obat.component.html',
  styleUrls: ['./obat.component.css']
})
export class InputObatComponent implements OnInit {

  items: Array<WizardItem>;
  isLoading: boolean;
  isSuccess: boolean;
  registrationId: string;
  registrasiModel: RegistrasiPelayananPasienModel;

  message: MessageType;
  tanggalPelayananMetaData: InputTextItem;
  obatMetaData: InputTextItem;
  jenisRacikanMetaData: InputTextItem;
  jumlahMetaData: InputTextItem;
  signaMetaData: InputTextItem;
  keteranganMetaData: InputTextItem;
  listObat: Array<RegistrasiPelayananObatModel> = new Array<RegistrasiPelayananObatModel>();
  isEdit: boolean;

  currentModel: RegistrasiPelayananObatModel;
  currentModelDetail: RegistrasiPelayananObatDetailModel;
  add() {
    this.currentModel.details.push(this.currentModelDetail);
    this.currentModelDetail = new RegistrasiPelayananObatDetailModel();
  }
  delete(item: RegistrasiPelayananObatDetailModel) {
    this.currentModel.details = this.currentModel.details.filter((v, i) => v.id !== item.id);
  }
  cancle() {
    this.currentModelDetail = new RegistrasiPelayananObatDetailModel();
    this.isEdit = false;
  }
  selected(item: RegistrasiPelayananObatDetailModel) {
    this.currentModelDetail = item;
    this.isEdit = true;
  }
  constructor(private notification: PushNotificationService, private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    this.tanggalPelayananMetaData = new InputTextItem("Tanggal Pelayanan ", "Isi dengan tanggal pelayanan", "datetime");
    this.obatMetaData = new InputTextItem("Nama Obat ", "Isi dengan nama obat", "select");
    this.jumlahMetaData = new InputTextItem("Jumlah Obat", "Isi dengan jumlah obat", "number");
    this.jumlahMetaData.required = true;
    this.keteranganMetaData = new InputTextItem("Keterangan ", "Isi dengan keterangan", "textarea");
    this.keteranganMetaData.required = true;
    this.obatMetaData.required = true;
    this.obatMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Obat/Search",
      fields: { title: 'namaObat', description: 'Nama Obat' },
      searchFullText: true
    };
    this.jenisRacikanMetaData = new InputTextItem("Jenis racikan ", "Isi dengan jenis racikan", "select");
    this.jenisRacikanMetaData.required = true;
    this.jenisRacikanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "JenisRacikan/Search",
      fields: { title: 'namaJenisRacikan', description: 'Jenis Racikan' },
      searchFullText: true
    };
    this.signaMetaData = new InputTextItem("Keterangan Pakai ", "Isi dengan keterangan pakai", "select");
    this.signaMetaData.required = true;
    this.signaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Signa/Search",
      fields: { title: 'namaSigna', description: 'Signa' },
      searchFullText: true
    };
    this.currentModel = new RegistrasiPelayananObatModel();
    this.currentModelDetail = new RegistrasiPelayananObatDetailModel();
    this.message = new MessageType();
    this.route.params.subscribe(params => {
      if (params.id !== undefined) {
        this.pasienService.getDetailObat(params.id).subscribe(result => {
          
          this.currentModel = result.data;
        });
      }

    });
    let parent = this.route.parent;
    parent.params.subscribe(params => {
      this.registrationId = params.id;
      if (this.registrationId !== undefined)
        pasienService.getRegistrasiPelayanan(this.registrationId).subscribe(result => {
          this.registrasiModel = result.data;
          this.currentModel.registrasiPasien = this.registrasiModel;
          this.currentModel.pasien = this.registrasiModel.pasien;
        });
    })
  }

  ngOnInit() {
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Completed();
    wizard[1].Completed();
    let item = new WizardItem("p", "Input Obat", "Digunakan untuk memasukan Pleayanan Obat  ", true, true);
    item.Active();
    wizard[2] = item;
    this.items = wizard;
  }

  saveObat() {
    if ((this.currentModel.IsValid !== undefined && this.currentModel.IsValid()) || this.currentModel.IsValid===undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.pasienService.saveObat(this.currentModel).subscribe(
        result => {
          let self = this;
          if (result.statusCode === 200) {
            this.isSuccess = true;
            this.notification.receive.subscribe(function (e) {
              self.currentModel.noResep = e.NoResep;
              self.message.content = "No Resep : " + e.NoResep;
              setTimeout(function () {
                self.message.type = undefined;
                self.notification.unsubscribe = result.data.id;
              }, 5000);
            });
            this.notification.subscribe = result.data.id;
            this.currentModel = new RegistrasiPelayananObatModel();

          }
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }

}
