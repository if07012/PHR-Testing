import { Component, OnInit } from '@angular/core';
import { Paged } from 'app/models/paged';
import { Observable } from 'rxjs/Rx';
import { ResultPaged } from 'app/models/registrasi.pelayanan.pasien';
import { RegistrasiPelayananObatModel } from '../../../../models/registrasi.pelayanan.obat';
import { MessageType } from 'app/models/messageType';
import { LampiranRegistrasiFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { PatientService } from 'app/services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'environments/environment';

@Component({
  selector: 'phr-daftar',
  templateUrl: './daftar.component.html',
  styleUrls: ['./daftar.component.css']
})
export class DaftarObatComponent implements OnInit {

  pages: Array<Paged>;
  listObat: Observable<ResultPaged<RegistrasiPelayananObatModel>>;
  message: MessageType;
  isLoading: boolean;
  list: Array<RegistrasiPelayananObatModel>;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: LampiranRegistrasiFilter;
  dariTanggalMetaData: InputTextItem;
  jenisLampiranMetaData: InputTextItem;
  sampaiTanggalMetaData: InputTextItem;

  constructor(private router: Router,private service: PatientService, private route: ActivatedRoute) {
    let parent = this.route.parent;
    this.message = new MessageType()
    this.filter = new LampiranRegistrasiFilter();
    this.filter.dariTanggal = new Date();
    this.filter.sampaiTanggal = new Date();
    parent.params.subscribe(params => {
      this.filter.registrationId = params.id;
    });
    this.dariTanggalMetaData = new InputTextItem("Dari tanggal", "dd-MM-yyyy", "datetime");
    this.sampaiTanggalMetaData = new InputTextItem("Sampai tanggal", "dd-MM-yyyy", "datetime");
    this.jenisLampiranMetaData = new InputTextItem("Jenis Lampiran", "", "select");
    
  }

  viewDetail(item:RegistrasiPelayananObatModel)
  {
    this.router.navigate(["home", "pelayanan", this.filter.registrationId, 'obat', "edit",item.id]);
  }
  ngOnInit() {
    this.findData();
  }

  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";

    this.listObat = this.service.getPagedObatRegistrasi(this.filter);

    this.listObat.subscribe(n => {
      this.list = new Array<RegistrasiPelayananObatModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }


}
