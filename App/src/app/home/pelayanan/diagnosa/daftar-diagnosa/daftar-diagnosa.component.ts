import { Component, OnInit } from '@angular/core';
import { MessageType } from 'app/models/messageType';
import { DaftarDiagnosaFilter } from "app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter";
import { Paged } from 'app/models/paged';
import { Observable } from 'rxjs/Observable';
import { PatientService } from 'app/services/patient.service';
import { RegistrasiPelayananDiagnosaModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { ActivatedRoute } from '@angular/router';
import { ResultPaged } from "app/models/registrasi.pelayanan.pasien";

@Component({
  selector: 'phr-daftar-diagnosa',
  templateUrl: './daftar-diagnosa.component.html',
  styleUrls: ['./daftar-diagnosa.component.css']
})
export class DaftarDiagnosaComponent implements OnInit {
  pages: Array<Paged>;
  listDiagnosa: Observable<ResultPaged<RegistrasiPelayananDiagnosaModel>>;
  message: MessageType;
  isLoading: boolean;
  list: Array<RegistrasiPelayananDiagnosaModel>;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: DaftarDiagnosaFilter;

  constructor(private service: PatientService, private route: ActivatedRoute) {
    let parent = this.route.parent;
    this.message = new MessageType()
    this.filter = new DaftarDiagnosaFilter();
    parent.params.subscribe(params => {
      this.filter.registrationId = params.id;
    });
  }


  ngOnInit() {
    this.findData();
  }

  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";

    this.listDiagnosa = this.service.getPagedRegistrasiPelayananDiagnosa(this.filter);

    this.listDiagnosa.subscribe(n => {
      this.list = new Array<RegistrasiPelayananDiagnosaModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }
}
