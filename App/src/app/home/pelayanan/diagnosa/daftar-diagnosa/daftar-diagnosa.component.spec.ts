import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarDiagnosaComponent } from './daftar-diagnosa.component';

describe('DaftarDiagnosaComponent', () => {
  let component: DaftarDiagnosaComponent;
  let fixture: ComponentFixture<DaftarDiagnosaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarDiagnosaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarDiagnosaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
