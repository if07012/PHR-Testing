import { Component, OnInit, Type } from '@angular/core';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { DaftarPelayananComponent } from "app/home/pelayanan/daftar/daftar.component";
import { PatientService } from 'app/services/patient.service';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { environment } from 'environments/environment';
import { RegistrasiPelayananDiagnosaModel } from "app/models/registrasi.pelayanan.diagnosa";
import { MessageType } from 'app/models/messageType';
import { InputTextComponent } from '../../../components/input-text/input-text.component';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { AppSettings } from 'app/helper/appsetting';

@Component({
  selector: 'phr-diagnosa',
  templateUrl: './diagnosa.component.html',
  styleUrls: ['./diagnosa.component.scss']
})
export class DiagnosaComponent implements OnInit {
  items: Array<WizardItem>;
  registrationId: string;
  isSuccess: boolean;
  isLoading: boolean;
  registrasiModel: RegistrasiPelayananPasienModel;
  tanggalPelayanan: InputTextItem;
  currentModel: RegistrasiPelayananDiagnosaModel;
  diagnosaMetaData: InputTextItem;
  dokterMetaData: InputTextItem;
  keteranganMetadata:InputTextItem;
  message: MessageType;
  constructor(private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    let parent = this.route.parent;
    this.currentModel = new RegistrasiPelayananDiagnosaModel();
    this.message = new MessageType();
    this.registrasiModel = new RegistrasiPelayananPasienModel();
    this.dokterMetaData = new InputTextItem("Dokter ", "Isi dengan Nama Dokter", "select");
    this.keteranganMetadata = new InputTextItem("Keterangan ", "Isi dengan Penjelanan dari diagnosa", "textarea");
    this.diagnosaMetaData = new InputTextItem("Diagnosa ", "Isi dengan Nama Diagnosa", "select");
    
    this.diagnosaMetaData.required = true;
    this.diagnosaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Diagnosa/Search",
      fields: { title: 'namaDiagnosa', description: 'Diagnosa' },
      searchFullText: true
    };


    this.tanggalPelayanan = new InputTextItem("Tanggal Pelayanan", "Isi dengan Tanggal Pelayanan", "datetime");
    this.tanggalPelayanan.required = true;
    this.currentModel.tanggalPelayanan = new Date();
    parent.params.subscribe(params => {
      this.registrationId = params.id;
      if (this.registrationId !== undefined)
        pasienService.getRegistrasiPelayanan(this.registrationId).subscribe(result => {
          this.registrasiModel = result.data;

          this.dokterMetaData = new InputTextItem("Dokter ", "Isi dengan Nama Dokter", "select");
          this.dokterMetaData.data = {
            url: environment.SYSADMIN_ENDPOINT + "Dokter/ByTanggalAndRuangan/" + this.registrasiModel.ruangan.id + "/" + this.getDate() + "/" + this.getDate(),
            fields: { title: 'namaPegawai', description: 'Dokter' },
            searchFullText: true
          };
          this.currentModel.dokter = this.registrasiModel.dokter;
          this.currentModel.registrasiPasien = this.registrasiModel;
          this.currentModel.pasien = this.registrasiModel.pasien;
        });
    })
  }
  getDate(): string {
    if (this.currentModel.tanggalPelayanan === undefined)
      this.currentModel.tanggalPelayanan = new Date();
    return this.currentModel.tanggalPelayanan.getFullYear() + "-" + (this.currentModel.tanggalPelayanan.getMonth() + 1) + "-" + this.currentModel.tanggalPelayanan.getDate();
  }
  back() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
  changeDokter(): void {
    if (this.currentModel.tanggalPelayanan === undefined || this.registrasiModel.ruangan === undefined) {
      return;
    }
    this.dokterMetaData = new InputTextItem("Dokter ", "Isi dengan Nama Dokter", "select");
    this.dokterMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Dokter/ByTanggalAndRuangan/" + this.registrasiModel.ruangan.id + "/" + this.getDate() + "/" + this.getDate(),
      fields: { title: 'namaPegawai', description: 'Dokter' },
      searchFullText: true
    };
  }
  ngOnInit() {
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Completed();
    wizard[1].Completed();
    let item =new WizardItem("p","Diagnosa","Digunakan untuk memberikan pelayanan Diagnosa  ",true,true);
    item.Active();
    wizard[2]=item;
    this.items = wizard;
  }

  saveDiagnosa() {
    if (this.currentModel.IsValid()) {
      this.isLoading = true;
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.pasienService.saveDiagnosa(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {
            this.isSuccess = true;

          }
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.isLoading = false;
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }

}
