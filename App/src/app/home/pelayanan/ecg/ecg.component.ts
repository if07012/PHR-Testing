import { Component, OnInit } from '@angular/core';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { RegistrasiPelayananDiagnosaModel, RegistrasiPelayananEcgModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { MessageType } from 'app/models/messageType';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from 'app/services/patient.service';
import { environment } from 'environments/environment';
import { AppSettings } from 'app/helper/appsetting';

@Component({
  selector: 'phr-ecg',
  templateUrl: './ecg.component.html',
  styleUrls: ['./ecg.component.scss']
})
export class EcgComponent implements OnInit {

  items: Array<WizardItem>;
  registrationId: string;
  isSuccess: boolean;
  isLoading: boolean;
  registrasiModel: RegistrasiPelayananPasienModel;
  currentModel: RegistrasiPelayananEcgModel;
  tanggalPelayanan: InputTextItem;
  diagnosaMetaData: InputTextItem;
  instruksiMetadata: InputTextItem;
  parent: ActivatedRoute;
  message: MessageType;
  constructor(private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    this.parent = this.route.parent;
    this.currentModel = new RegistrasiPelayananEcgModel();
    this.message = new MessageType();
    this.registrasiModel = new RegistrasiPelayananPasienModel();
    this.instruksiMetadata = new InputTextItem("Instruksi ", "Isi dengan Penjelanan dari diagnosa", "textarea");
    this.diagnosaMetaData = new InputTextItem("Diagnosa ", "Isi dengan Nama Diagnosa", "textarea");


    this.tanggalPelayanan = new InputTextItem("Tanggal Pelayanan", "Isi dengan Tanggal Pelayanan", "datetime");
    this.tanggalPelayanan.required = true;
    this.currentModel.tanggalPelayanan = new Date();
    route.params.subscribe(params => {
      debugger;
      if (params.id !== undefined && params.id !== null && params.id !== '') {
        this.pasienService.getEcgById(params.id).subscribe(result => {
          this.currentModel.diagnosa = result.data.diagnosa;
          this.currentModel.dokter = result.data.dokter;
          this.currentModel.id = result.data.id;
          this.currentModel.instruksi = result.data.instruksi;
          this.getDetailRegistrasiPelayanan();
        });
      }
      else
        this.getDetailRegistrasiPelayanan();
    });

  }
  getDetailRegistrasiPelayanan() {
    this.parent.params.subscribe(params => {
      this.registrationId = params.id;
      if (this.registrationId !== undefined)
        this.pasienService.getRegistrasiPelayanan(this.registrationId).subscribe(result => {
          this.registrasiModel = result.data;
          this.currentModel.dokter = this.registrasiModel.dokter;
          this.currentModel.registrasiPasien = this.registrasiModel;
          this.currentModel.pasien = this.registrasiModel.pasien;

        });
    });
  }
  getDate(): string {
    if (this.currentModel.tanggalPelayanan === undefined)
      this.currentModel.tanggalPelayanan = new Date();
    return this.currentModel.tanggalPelayanan.getFullYear() + "-" + (this.currentModel.tanggalPelayanan.getMonth() + 1) + "-" + this.currentModel.tanggalPelayanan.getDate();
  }
  back() {
    this.router.navigate(['../../'], { relativeTo: this.route });
  }
  changeDokter(): void {
    if (this.currentModel.tanggalPelayanan === undefined || this.registrasiModel.ruangan === undefined) {
      return;
    }
  }
  ngOnInit() {
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Completed();
    wizard[1].Completed();
    let item = new WizardItem("p", "ECG", "Digunakan untuk memberikan pelayanan ECG", true, true);
    item.Active();
    wizard[2] = item;
    this.items = wizard;
  }
  sendSms() {
    this.pasienService.sendNotifEcgById(this.currentModel.id).subscribe(result => {
    });
  }
  saveDiagnosa() {
    if (this.currentModel.IsValid()) {
      this.isLoading = true;
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.pasienService.saveEcg(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {
            this.isSuccess = true;

          }
          this.currentModel.id = result.data.id;
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.isLoading = false;
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }
}
