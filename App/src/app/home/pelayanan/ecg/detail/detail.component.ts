import { Component, OnInit, AfterViewInit, ElementRef } from '@angular/core';
import { PatientService } from 'app/services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageType } from 'app/models/messageType';
import { environment } from 'environments/environment';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { DetailEcg, BufferEcgModel } from 'app/models/registrasi.pelayanan.pasien';
declare var window: any;
declare var $: any;
declare var kendo: any;

@Component({
  selector: 'phr-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailEcgComponent implements OnInit, AfterViewInit {
  print(el: any) {
    kendo.drawing.drawDOM($(el.currentTarget).parent().parent().parent()).then(function (group) {
      kendo.drawing.pdf.saveAs(group, "export9 vvb.pdf");
    });
  }
  ngAfterViewInit(): void {


  }
  detailEcg: DetailEcg;
  message: MessageType;
  namaSignalMetaData: InputTextItem;
  registrationId: string;
  pelayananEcgId: string;
  constructor(private service: PatientService, private route: ActivatedRoute, private router: Router) {
    this.namaSignalMetaData = new InputTextItem("Jenis ECG ", "Isi dengan Jenis Ecg", "select");
    this.namaSignalMetaData.hideTitle = true;
    this.namaSignalMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "JenisSignalEcg/Search",
      fields: { title: 'namaSignal', description: 'Signal' },
      searchFullText: true
    };

    let parent = this.route.parent;
    parent.params.subscribe(params => {
      this.registrationId = params.id;
    });
    this.route.params.subscribe(params => {
      this.pelayananEcgId = params.id;
    });
  }
  edit(item: any) {
    if (item.isEdit === undefined)
      item.isEdit = true;
    else if (item.isEdit === false)
      item.isEdit = true;
    else if (item.isEdit === true)
      item.isEdit = false;
  }

  save(item: BufferEcgModel) {
    item.isLoading = true;
    item.jenisSignalId = item.signal.id;
    item.namaJenisSignal = item.signal.namaSignal;
    this.service.updateEcgDetail(item).subscribe(result => {
      item.message = new MessageType();
      item.message.title = "Update";
      item.message.content = "Data berhaslil di simpan";
      setTimeout(() => {
        item.message = undefined;
      },3000);
      item.isLoading = false;
    });
  }
  ngOnInit() {
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.detailEcg = new DetailEcg();
    this.service.detailEcg(this.pelayananEcgId).subscribe(result => {
      this.detailEcg = result.data;
      this.message = new MessageType();
    });
  }

}
