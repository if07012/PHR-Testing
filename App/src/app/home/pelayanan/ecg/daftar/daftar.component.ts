import { Component, OnInit, AfterViewInit, Directive } from '@angular/core';
import { Paged } from 'app/models/paged';
import { MessageType } from 'app/models/messageType';
import { RegistrasiPelayananDiagnosaModel, RegistrasiPelayananEcgModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { DaftarDiagnosaFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { PatientService } from 'app/services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { ResultPaged } from 'app/models/registrasi.pelayanan.pasien';
import { PopUpDirective } from "app/components/popup/pop-up.directive";
declare var $: any;
@Component({
  selector: 'phr-daftar',
  templateUrl: './daftar.component.html',
  styleUrls: ['./daftar.component.scss'],
  providers: [PopUpDirective]
})
export class DaftarEcgComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    $('.heartbeat').each(function () {
      $(this)
        .popup({
          on: 'hover',
          variation: 'small inverted',
          exclusive: true,
          content: $(this).attr('class')
        })
        ;
    });
  }
  pages: Array<Paged>;
  listEcg: Observable<ResultPaged<RegistrasiPelayananEcgModel>>;
  message: MessageType;
  isLoading: boolean;
  list: Array<RegistrasiPelayananEcgModel>;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: DaftarDiagnosaFilter;

  constructor(private service: PatientService, private route: ActivatedRoute, private router: Router) {

    this.message = new MessageType()
    this.filter = new DaftarDiagnosaFilter();
    let parent = this.route.parent;
    parent.params.subscribe(params => {
      this.filter.registrationId = params.id;
    });
  }
  showDetail(item: RegistrasiPelayananEcgModel) {
    this.router.navigate(["home", "pelayanan", this.filter.registrationId, 'detail', "ecg", item.id]);
  }
  editLayanan(item: RegistrasiPelayananEcgModel) {
    this.router.navigate(["home", "pelayanan", this.filter.registrationId, 'edit', "ecg", item.id]);
  }
  ngOnInit() {
    this.findData();
  }

  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";

    this.listEcg = this.service.getPagedRegistrasiPelayananEcg(this.filter);

    this.listEcg.subscribe(n => {
      this.list = new Array<RegistrasiPelayananEcgModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;
      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }

}
