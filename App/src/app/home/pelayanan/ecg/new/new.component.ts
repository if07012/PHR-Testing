import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MessageType } from 'app/models/messageType';
import { ActivatedRoute, Router } from '@angular/router';
import { PatientService } from 'app/services/patient.service';
import { NewEcgFilter } from './model/new.ecg.filter';
import { Paged } from 'app/models/paged';
import { Observable } from 'rxjs/Observable';
import { ResultPaged, RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { BufferEcgModel, MappingBufferEcg } from '../../../../models/registrasi.pelayanan.pasien';
import { RegistrasiPelayananEcgModel } from '../../../../models/registrasi.pelayanan.diagnosa';
import { DaftarEcgFilter } from '../../../pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { RegistrasiPasienModel } from '../../../../models/registrasi.pasien';
import { PushNotificationService } from 'app/services/push-notification.service';
import { Pasien } from 'app/models/pasien';
declare var $: any;
@Component({
  selector: 'phr-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.scss']
})
export class NewEcgComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {

  }
  tanggalPelayanan: InputTextItem;
  diagnosaMetaData: InputTextItem;
  instruksiMetadata: InputTextItem;
  isAdd: boolean;
  messageSave: MessageType;
  message: MessageType;
  messagePelayanan: MessageType;
  isLoading: boolean;
  filter: NewEcgFilter;
  pages: Array<Paged>;
  listEcg: Observable<ResultPaged<BufferEcgModel>>;
  list: Array<BufferEcgModel>;
  isSuccess: boolean;
  listPelayanan: Array<RegistrasiPelayananEcgModel>;
  selectedPelayanan: RegistrasiPelayananEcgModel;
  currentModel: RegistrasiPelayananPasienModel;
  pageList: number[] = [5, 10, 25, 50, 100];
  registrasiId: string;
  changePelayanan(e: RegistrasiPelayananEcgModel) {
    if (this.isAdd)
      return;
    if (this.selectedPelayanan === e) {
      $('.selected').visibility('destroy');
      this.selectedPelayanan = undefined;
      return;
    }
    this.selectedPelayanan = e;
    $('.selected').visibility('destroy');
    setTimeout(function () {
      $('.selected').each(function () {
        var ctrl = $(this)[0];
        $(this).css({ 'width': ctrl.offsetWidth });
      })
      $('.selected').visibility({
        type: 'fixed',
        offset: 80
      });
    })
  }
  constructor(private notification: PushNotificationService, private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    let parent = this.route.parent;
    this.selectedPelayanan = new RegistrasiPelayananEcgModel();
    this.filter = new NewEcgFilter();
    this.messagePelayanan = new MessageType();
    this.messagePelayanan.type = "loading";
    this.messagePelayanan.content = "Sedang proses....";
    this.messageSave = new MessageType();
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.instruksiMetadata = new InputTextItem("Instruksi ", "Isi dengan Penjelanan dari diagnosa", "textarea");
    this.diagnosaMetaData = new InputTextItem("Diagnosa ", "Isi dengan Nama Diagnosa", "textarea");


    this.tanggalPelayanan = new InputTextItem("Tanggal Pelayanan", "Isi dengan Tanggal Pelayanan", "datetime");
    this.tanggalPelayanan.required = true;
    parent.params.subscribe(params => {
      this.registrasiId = params.id;
      pasienService.getRegistrasiPelayanan(params.id).subscribe(result => {
        this.currentModel = result.data;
        this.filter.pasienId = result.data.pasien.id;
        this.notification.receive.subscribe(result => {
          this.findData();
        });
        console.log("Subscribe to id : " + result.data.pasien.id);
        this.notification.subscribe = result.data.pasien.id;
        this.message = new MessageType();
        this.findData();
        this.findPelayananData();
      });
    })
  }


  ngOnInit() {
    this.isAdd = false;
  }
  selectedBuffer(item: BufferEcgModel) {
    item.isSelected = !item.isSelected;
  }
  findPelayananData() {
    this.isLoading = true;
    this.messagePelayanan = new MessageType();
    this.messagePelayanan.type = "loading";
    this.messagePelayanan.content = "Sedang proses....";
    let filterPelayanan = new DaftarEcgFilter();
    filterPelayanan.registrationId = this.registrasiId;
    filterPelayanan.take = 3;
    this.pasienService.getPagedRegistrasiPelayananEcg(filterPelayanan).subscribe(n => {
      this.listPelayanan = new Array<RegistrasiPelayananEcgModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.listPelayanan.push(data);
      }

      if (this.listPelayanan.length > 0)
        this.messagePelayanan = new MessageType();
      else {
        this.messagePelayanan.type = "200";
        this.messagePelayanan.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.messagePelayanan.type = "404";
      this.messagePelayanan.content = "Terjadi kesalahan";
    });
  }

  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";

    this.filter.take = 1000;
    this.listEcg = this.pasienService.getNewEcg(this.filter);

    this.listEcg.subscribe(n => {
      this.list = new Array<BufferEcgModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  addPelayanan() {
    this.isAdd = !this.isAdd;
    if (this.isAdd)
      this.selectedPelayanan = new RegistrasiPelayananEcgModel();
    else
      this.selectedPelayanan = undefined;
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }
  saveData() {
    if (this.selectedPelayanan != null && this.selectedPelayanan.diagnosa !== null && this.selectedPelayanan.diagnosa !== '') {
      this.isLoading = true;
      this.messageSave.type = "loading";
      this.messageSave.content = "Sedang proses....";
      let mapping = new MappingBufferEcg();

      mapping.registrasiModel = this.selectedPelayanan;
      mapping.registrasiModel.registrasiPasien = this.currentModel;
      mapping.registrasiModel.pasien = this.currentModel.pasien;
      mapping.listBuffer = this.list.filter((v, i) => v.isSelected);
      mapping.listBuffer.forEach((v, i, a) => {
        v.isHide = true;

      });
      this.pasienService.mappingEcg(mapping).subscribe(
        result => {
          let self = this;
          if (result.statusCode === 200) {
            this.isSuccess = true;
            this.list = this.list.filter((v, i) => !v.isSelected);
            this.messageSave.type = result.statusCode.toString();
            this.messageSave.content = result.message.replace("\n", ",");
            this.notification.send(result.data.pasien.id, {});
            this.isAdd = false;
            this.findPelayananData();
          } else {
            mapping.listBuffer.forEach((v, i, a) => {
              v.isHide = false;

            });
          }

          this.isLoading = false;


          return result;

        },
        err => {
          this.isLoading = false;
          this.messageSave.type = "404";
          this.messageSave.content = "Terjadi kesalahan";
        });
    }
  }
}

