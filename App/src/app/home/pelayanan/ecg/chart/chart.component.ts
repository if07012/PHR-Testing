import { Component, OnInit, Input, ViewChild, ElementRef, DoCheck, AfterViewInit } from '@angular/core';
import { BufferEcgModel } from "app/models/registrasi.pelayanan.pasien";
import { MessageType } from 'app/models/messageType';
declare var window: any;
declare var $: any;
declare var document: any;

@Component({
  selector: 'phr-chart-ecg',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartEcgComponent implements OnInit, DoCheck, AfterViewInit {

  redraw() {
    $(this.container.nativeElement).css({ 'max-width': $(this.container.nativeElement).width() });
    let max = 0;
    let labels: Array<string> = new Array<string>();
    let datas: Array<number> = new Array<number>();
    var ctx = this.canvasElem.nativeElement.getContext("2d");
    let block = 20;

    let first = true;
    let arrayPoints: Array<number> = new Array<number>();
    if (this.item.decode == null) return
    let file = (this.item.decode).toString();
    let originalFile = JSON.parse(file) as Array<string>;
    let arrays: Array<string> = originalFile;
    let stoped: number = 0;
    let lastX = 0;
    for (let j in arrays) {
      if (+arrays[j] > max)
        max = +arrays[j];
      arrayPoints.push(+arrays[j]);
      first = false;
      lastX = +j;
    }
    let scale = 150 / max;

    $(this.canvasElem.nativeElement).attr('width', lastX + 40);
    $(this.canvasElem.nativeElement).draggable({ axis: "x" });
    let self = this;
    $(window).on('mouseup', function () {
      let left = $(self.canvasElem.nativeElement).position().left * -1 + $(self.container.nativeElement).width();
      if (left >= $(self.canvasElem.nativeElement).width()) {
        $(self.canvasElem.nativeElement).animate(
          { left: ($(self.canvasElem.nativeElement).width() - $(self.container.nativeElement).width() ) * -1 },
          500
        );
      }
      if ($(self.canvasElem.nativeElement).position().left > 0)
        $(self.canvasElem.nativeElement).animate(
          { left: 0 },
          500
        );
    });
    for (let i = 0; i < 9; i++) {
      ctx.beginPath();
      ctx.lineWidth = 0.5;
      ctx.moveTo(0, i * block);
      ctx.lineTo(5000, i * block);
      ctx.strokeStyle = "#ff0000";
      ctx.stroke();
    }
    for (let i = 0; i < 5000 / block; i++) {
      ctx.beginPath();
      ctx.moveTo(i * block, 0);
      ctx.lineTo(i * block, 160);

      ctx.stroke();
    }
    ctx.beginPath();
    first = true;
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
    for (let j in arrayPoints) {
      if (first)
        ctx.moveTo(+j, scale * (max + block - 5 - +arrayPoints[j]));
      else
        ctx.lineTo(+j, scale * (max + block - 5 - +arrayPoints[j]));
      first = false;
    }

    ctx.stroke();
  }
  ngAfterViewInit(): void {

    this.redraw();

  }
  item: BufferEcgModel;
  ngDoCheck(): void {
  }
  get data() {
    return this.item;
  }
  @Input()
  set data(val: BufferEcgModel) {
    this.item = val;
    if (val === undefined)
      return;
    if (this.canvasElem === undefined)
      return;
  }
  @ViewChild('canvasElem')
  canvasElem: ElementRef;
  @ViewChild('container')
  container: ElementRef;
  constructor() {
    let self = this;
    $(window).on('resize', function () {
      debugger;
      self.redraw();
    });
  }

  ngOnInit() {

  }

}
