import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CatatanKlinisComponent } from './catatan-klinis.component';

describe('CatatanKlinisComponent', () => {
  let component: CatatanKlinisComponent;
  let fixture: ComponentFixture<CatatanKlinisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatatanKlinisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatatanKlinisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
