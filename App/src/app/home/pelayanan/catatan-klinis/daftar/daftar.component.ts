import { Component, OnInit } from '@angular/core';
import { Paged } from 'app/models/paged';
import { MessageType } from 'app/models/messageType';
import { RegistrasiPelayananDiagnosaModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { DaftarDiagnosaFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { DaftarCatatanKlinisFilter } from '../../../pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { PatientService } from 'app/services/patient.service';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { ResultPaged } from 'app/models/registrasi.pelayanan.pasien';
import { RegistrasiPelayananCatatanKlinisModel } from '../../../../models/registrasi.pelayanan.catatan.klinis';

@Component({
  selector: 'phr-daftar',
  templateUrl: './daftar.component.html',
  styleUrls: ['./daftar.component.css']
})
export class DaftarCatatanKlinisComponent implements OnInit {
  pages: Array<Paged>;
  listDiagnosa: Observable<ResultPaged<RegistrasiPelayananCatatanKlinisModel>>;
  message: MessageType;
  isLoading: boolean;
  list: Array<RegistrasiPelayananCatatanKlinisModel>;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: DaftarCatatanKlinisFilter;

  constructor(private service: PatientService, private route: ActivatedRoute) {
    let parent = this.route.parent;
    this.message = new MessageType()
    this.filter = new DaftarCatatanKlinisFilter();
    parent.params.subscribe(params => {
      this.filter.registrationId = params.id;
    });
  }


  ngOnInit() {
    this.findData();
  }

  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";

    this.listDiagnosa = this.service.getPagedRegistrasiPelayananCatatanKlinis(this.filter);

    this.listDiagnosa.subscribe(n => {
      this.list = new Array<RegistrasiPelayananCatatanKlinisModel>();
      for (var i in n.records) {
        let data = n.records[i];
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }

}
