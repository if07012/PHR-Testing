import { Component, OnInit } from '@angular/core';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { AppSettings } from 'app/helper/appsetting';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { RegistrasiPelayananCatatanKlinisModel } from "app/models/registrasi.pelayanan.catatan.klinis";
import { MessageType } from 'app/models/messageType';
import { PatientService } from 'app/services/patient.service';
import { ActivatedRoute, Router } from '@angular/router';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';

@Component({
  selector: 'phr-catatan-klinis',
  templateUrl: './catatan-klinis.component.html',
  styleUrls: ['./catatan-klinis.component.scss']
})
export class CatatanKlinisComponent implements OnInit {
  items: Array<WizardItem>;
  isLoading:boolean;
  isSuccess:boolean;
  registrationId:string;
  registrasiModel: RegistrasiPelayananPasienModel;
  
  message: MessageType;
  tanggalPelayananMetaData: InputTextItem;
  beratBadanMetaData: InputTextItem;
  tinggiBadanMetaData: InputTextItem;
  suhuMetaData: InputTextItem;
  tekananDarahMetaData: InputTextItem;
  keluhanMetaData: InputTextItem;
  currentModel: RegistrasiPelayananCatatanKlinisModel;
  constructor(private router: Router, private route: ActivatedRoute, private pasienService: PatientService) {
    this.tanggalPelayananMetaData = new InputTextItem("Tanggal Pelayanan ", "Isi dengan tanggal pelayanan", "datetime");

    this.keluhanMetaData = new InputTextItem("Keluhan ", "Isi dengan keluhan", "textarea");
    this.beratBadanMetaData = new InputTextItem("Berat Badan ", "Isi dengan berat badan", "number");
    this.beratBadanMetaData.data = { min: 0, max: 200 }
    this.tinggiBadanMetaData = new InputTextItem("Tinggi Badan ", "Isi dengan tinggi badan", "number");
    this.tinggiBadanMetaData.data = { min: 20, max: 250 }
    this.suhuMetaData = new InputTextItem("Suhu ", "Isi dengan suhu ", "number");
    this.suhuMetaData.data = { min: 36, max: 42 }
    this.tekananDarahMetaData = new InputTextItem("Tekanan Darah ", "Isi dengan tekanan darah", "label");
    this.currentModel = new RegistrasiPelayananCatatanKlinisModel();
    this.message = new MessageType();

    let parent = this.route.parent;
     parent.params.subscribe(params => {
      this.registrationId = params.id;
      if (this.registrationId !== undefined)
        pasienService.getRegistrasiPelayanan(this.registrationId).subscribe(result => {
          this.registrasiModel = result.data;
          this.currentModel.registrasiPasien = this.registrasiModel;
          this.currentModel.pasien = this.registrasiModel.pasien;
        });
    })
  }

  ngOnInit() {
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Completed();
    wizard[1].Completed();
    let item = new WizardItem("p", "Catatan Klinis", "Digunakan untuk memasukan Catatan Klinis  ", true, true);
    item.Active();
    wizard[2] = item;
    this.items = wizard;
  }

  saveCatatanKlinis() {
    if (this.currentModel.IsValid()) {
      this.isLoading = true;
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.pasienService.saveCatatanKlinis(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {
            this.isSuccess = true;

          }
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.isLoading = false;
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }

}
