import { Observable } from 'rxjs/Rx';

import { Component, OnInit, AfterViewInit, HostBinding, state } from '@angular/core';
import { DaftarRegistrasiFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { Router } from '@angular/router';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { Paged } from 'app/models/paged';
import { MessageType } from 'app/models/messageType';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { CoreHttpService } from 'app/services/core-http.service';
import { PatientService } from 'app/services/patient.service';
import { StatusPendaftaran } from "app/models/status-pendaftaran";

@Component({
  selector: 'phr-daftar',
  templateUrl: './daftar.component.html',
  styleUrls: ['./daftar.component.scss']
})
export class DaftarPasienComponent implements OnInit {
  listStatusPanggil: Array<StatusPendaftaran> = new Array<StatusPendaftaran>();
  pages: Array<Paged>;
  message: MessageType;
  isLoading: boolean;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: DaftarRegistrasiFilter;
  dariTanggalMetaData: InputTextItem;
  noCmMetaData: InputTextItem;
  namaPasienMetaData: InputTextItem;
  sampaiTanggalMetaData: InputTextItem;
  listRegistrasiPelayanan: Observable<any>
  list: Array<RegistrasiPelayananPasienModel>;
  constructor(private http: CoreHttpService, private service: PatientService, private router: Router) {
    this.filter = new DaftarRegistrasiFilter();
    this.message = new MessageType();
    this.listStatusPanggil.push(new StatusPendaftaran("", "Registrasi Pelayanan"))
    this.listStatusPanggil.push(new StatusPendaftaran("", "Detail Pelayanan"))

  }

  ngOnInit() {
    this.dariTanggalMetaData = new InputTextItem("Dari tanggal", "dd-MM-yyyy", "datetime");
    this.sampaiTanggalMetaData = new InputTextItem("Sampai tanggal", "dd-MM-yyyy", "datetime");
    this.noCmMetaData = new InputTextItem("No. Cm/ Nama Pasien", "", "string");
    this.namaPasienMetaData = new InputTextItem("NamaPasien", "", "string");
    this.filter.dariTanggal = new Date();

    this.filter.sampaiTanggal = new Date();
    this.findData();

  }
  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.filter.namaPasien = this.filter.noCm;
    this.listRegistrasiPelayanan = this.service.getPagedPasien(this.filter);

    this.listRegistrasiPelayanan.subscribe(n => {
      this.list = new Array<RegistrasiPelayananPasienModel>();
      for (var i in n.records) {
        let data = n.records[i];
        data.actions = this.listStatusPanggil;
        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }

  changeState(model: any, state: StatusPendaftaran) {
    this.router.navigate(['/home/registrasi-pelayanans/'+ model.id ]);
  }
}
