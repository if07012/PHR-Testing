import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'environments/environment';
import { CoreHttpService } from 'app/services/core-http.service';

@Component({
  selector: 'phr-jadwal-dokter',
  templateUrl: './jadwal-dokter.component.html',
  styleUrls: ['./jadwal-dokter.component.scss']
})
export class JadwalDokterComponent implements OnInit {

  @Input()
  item: any;
  date: Date;
  @Input()
  get tanggalRegistrasi() {
    return this.date;
  }
  set tanggalRegistrasi(val) {
    this.date = val;
  }
  constructor(private coreHttp: CoreHttpService) { }

  ngOnInit() {
    let month: string = (this.tanggalRegistrasi.getMonth() + 1).toString();
    if ((this.tanggalRegistrasi.getMonth() + 1) < 10)
      month = "0" + month;
    let date = this.tanggalRegistrasi.getFullYear() + "-" + (month) + "-" + this.tanggalRegistrasi.getDate();
    this.coreHttp.Get(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/CheckAntrian/" + this.item.id + "/" + date + "/" + this.item.idRuangan).subscribe(result => {
      this.item.noLast = result.data;
    });
  }

}
