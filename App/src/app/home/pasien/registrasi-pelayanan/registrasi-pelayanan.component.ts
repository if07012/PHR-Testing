import { Component, OnInit } from '@angular/core';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { AppSettings } from 'app/helper/appsetting';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { environment } from 'environments/environment';
import { RegistrasiPelayananPasienModel } from "app/models/registrasi.pelayanan.pasien";
import { Alamat } from 'app/models/alamat';
import { Pasien } from 'app/models/pasien';
import { MessageType } from 'app/models/messageType';
import { PushNotificationService } from 'app/services/push-notification.service';
import { PatientService } from 'app/services/patient.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CoreHttpService } from 'app/services/core-http.service';

@Component({
  selector: 'phr-registrasi-pelayanan',
  templateUrl: './registrasi-pelayanan.component.html',
  styleUrls: ['./registrasi-pelayanan.component.scss']
})
export class RegistrasiPelayananComponent implements OnInit {
  pasienMetaData: InputTextItem;
  tanggalRegistrasi: InputTextItem;
  ruanganMetaData: InputTextItem;
  dokterMetaData: InputTextItem;
  isLoading: boolean;
  message: MessageType;
  isShowDokter: boolean;
  currentModel: RegistrasiPelayananPasienModel;
  items: Array<WizardItem>;
  isSuccess: boolean;
  state: string;
  listDokter: Array<any> = new Array<any>();  
  noLast:string;
  constructor(private coreHttp: CoreHttpService, private patientService: PatientService,
    private notification: PushNotificationService,
    private router: Router,
    private route: ActivatedRoute) {
    let alamat = new Alamat();
    let pasien = new Pasien();
    this.currentModel = new RegistrasiPelayananPasienModel();
    this.currentModel.pasien = pasien;
    this.message = new MessageType();
  }

  ngOnInit() {
    this.currentModel.tanggalRegistrasi = new Date();
    this.route.params.subscribe(n => {
      this.coreHttp.Get(environment.PATIENT_ENDPOINT + "Pasien/" + n.id).subscribe(pasien => {
        this.currentModel.pasien = pasien;
      });
    });
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Disabled();
    wizard[1].Active();
    this.items = wizard;
    this.pasienMetaData = new InputTextItem("Nama Pasien", "Isi dengan Nama Pasien", "select");
    this.pasienMetaData.required = true;
    this.pasienMetaData.data = {
      url: environment.PATIENT_ENDPOINT + "Pasien/Search",
      fields: { title: 'namaPasien', description: 'Agama' },
      searchFullText: true
    };

    this.tanggalRegistrasi = new InputTextItem("Tanggal Registrasi", "Isi dengan Tanggal registrasi", "datetime");
    this.tanggalRegistrasi.required = true;

    this.ruanganMetaData = new InputTextItem("Ruangan Tujuan", "Isi dengan Nama Ruangan dituju", "select");
    this.ruanganMetaData.required = true;
    this.ruanganMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Ruangan/Pelayanan",
      fields: { title: 'namaRuangan', description: 'Ruangan' },
      searchFullText: true
    };

    this.dokterMetaData = new InputTextItem("Dokter ", "Isi dengan Nama Dokter", "select");
  }
  changeDokter(): void {
    this.isShowDokter = false;
    if (this.currentModel.tanggalRegistrasi === undefined || this.currentModel.ruangan === undefined) {
      return;
    }
    this.isShowDokter = true;
    let date = this.currentModel.tanggalRegistrasi.getFullYear() + "-" + (this.currentModel.tanggalRegistrasi.getMonth() + 1) + "-" + this.currentModel.tanggalRegistrasi.getDate();
    this.dokterMetaData = new InputTextItem("Dokter ", "Isi dengan Nama Dokter", "select");
    this.dokterMetaData.required = true;
    this.dokterMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Dokter/ByTanggalAndRuangan/" + this.currentModel.ruangan.id + "/" + date + "/" + date,
      fields: { title: 'namaPegawai', description: 'Dokter' },
      searchFullText: true
    };
    this.isLoading=true;
    this.state = "dokter";
    this.coreHttp.Get(environment.SYSADMIN_ENDPOINT + "Dokter/ByTanggalAndRuangan/" + this.currentModel.ruangan.id + "/" + date + "/" + date + "/-").subscribe(result => {
      this.listDokter = result;
      this.isLoading=false;
    });
  }

  savePasien(): void {

    if (this.currentModel.IsValid()) {
      this.isLoading = true;
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.patientService.saveRegistrasiPelayanan(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {
            this.isSuccess = true;
            this.notification.receive.subscribe(function (e) {
              self.currentModel.pasien.noRekamMedis = e.NoRekamMedis;
              self.message.content = "No Registrasi : " + e.NoRegistrasi;
              setTimeout(function () {
                self.message.type = undefined;
                self.notification.unsubscribe = result.data.id;
              }, 5000);
            });
            this.notification.subscribe = result.data.id;

          }
          this.message.type = result.statusCode.toString();
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.isLoading = false;
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }
}
