import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaftarRegistrasiComponent } from './daftar-registrasi.component';

describe('DaftarRegistrasiComponent', () => {
  let component: DaftarRegistrasiComponent;
  let fixture: ComponentFixture<DaftarRegistrasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaftarRegistrasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaftarRegistrasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
