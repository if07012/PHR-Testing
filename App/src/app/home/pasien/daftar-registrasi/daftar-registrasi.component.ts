import { Component, OnInit, AfterViewInit, HostBinding, state } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { CoreHttpService } from 'app/services/core-http.service';
import { DaftarRegistrasiFilter } from './model/daftar.registrasi-filter';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { environment } from '../../../../environments/environment';
import { PatientService } from 'app/services/patient.service';
import { Router } from '@angular/router';
import { Paged } from "app/models/paged";
import { MessageType } from 'app/models/messageType';
import { StatusPendaftaran } from '../../../models/status-pendaftaran';
import { slideInDownAnimation } from 'app/animations/slideInDownAnimation';

declare var $: any;
@Component({
  selector: 'phr-daftar-registrasi',
  templateUrl: './daftar-registrasi.component.html',
  styleUrls: ['./daftar-registrasi.component.scss'],
  animations: [slideInDownAnimation]
})
export class DaftarRegistrasiComponent implements OnInit, AfterViewInit {
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display') display = 'block';
  ngAfterViewInit(): void {

  }
  pages: Array<Paged>;
  message: MessageType;
  isLoading: boolean;
  pageList: number[] = [5, 10, 25, 50, 100];
  filter: DaftarRegistrasiFilter;
  listStatusPanggil: Array<StatusPendaftaran> = new Array<StatusPendaftaran>();
  listStatusAntrian: Array<StatusPendaftaran> = new Array<StatusPendaftaran>();
  dariTanggalMetaData: InputTextItem;
  noCmMetaData: InputTextItem;
  namaPasienMetaData: InputTextItem;
  sampaiTanggalMetaData: InputTextItem;
  listRegistrasiPelayanan: Observable<any>
  list: Array<RegistrasiPelayananPasienModel>;
  constructor(private http: CoreHttpService, private service: PatientService, private router: Router) {
    this.filter = new DaftarRegistrasiFilter();
    this.message = new MessageType();
    this.listStatusPanggil.push(new StatusPendaftaran("", "Pelayanan"))
    this.listStatusPanggil.push(new StatusPendaftaran("", "Detail Pasien"))
    this.listStatusAntrian.push(new StatusPendaftaran("", "Panggil"))
    this.listStatusAntrian.push(new StatusPendaftaran("", "Batal"))
  }

  ngOnInit() {
    this.dariTanggalMetaData = new InputTextItem("Dari tanggal", "dd-MM-yyyy", "datetime");
    this.sampaiTanggalMetaData = new InputTextItem("Sampai tanggal", "dd-MM-yyyy", "datetime");
    this.noCmMetaData = new InputTextItem("No. Cm/ Nama Pasien", "", "string");
    this.namaPasienMetaData = new InputTextItem("NamaPasien", "", "string");
    this.filter.dariTanggal = new Date();
  
    this.filter.sampaiTanggal = new Date();
    this.findData();

  }
  changeState(model: RegistrasiPelayananPasienModel, state: StatusPendaftaran) {
    model.isLoading = !model.isLoading;
    let action = false;
    if (state.namaStatus === "Panggil") {
      model.idStatus = 1;
      action = true;
    }
    else if (state.namaStatus === "Batal") {
      model.idStatus = 2;
      action = true;
    }
    if (action == true) {
      this.service.updateStatusRegistrasiPelayanan(model).subscribe(n => {
        model.isLoading = !model.isLoading;
        model.statusPendaftaran = n.data.statusPendaftaran;
        if (state.namaStatus == "Panggil") {
          model.actions = this.listStatusPanggil;
        }
      });
    } else if (state.namaStatus == "Pelayanan") {
      this.router.navigate(['/home/pelayanan', model.id]);
    }

  }
  findData() {
    this.isLoading = true;
    this.message = new MessageType();
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.filter.namaPasien = this.filter.noCm;
    this.listRegistrasiPelayanan = this.service.getPagedRegistrasiPelayanan(this.filter);

    this.listRegistrasiPelayanan.subscribe(n => {
      this.list = new Array<RegistrasiPelayananPasienModel>();
      for (var i in n.records) {
        let data = n.records[i];
        if (data.idStatusPendaftaran == 1) {
          data.actions = this.listStatusPanggil;
        } else {
          data.actions = this.listStatusAntrian;
        }

        this.list.push(data);
      }
      this.pages = new Array<Paged>();
      let start = n.currentPage;
      this.filter.take = n.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = n.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * n.take < n.totalRecords)
          this.pages.push(new Paged(i, i == n.currentPage));
      }
      this.isLoading = false;

      if (this.list.length > 0)
        this.message = new MessageType();
      else {
        this.message.type = "200";
        this.message.content = "Data tidak ditemukan";
      }
    }, err => {
      this.isLoading = false;
      this.message.type = "404";
      this.message.content = "Terjadi kesalahan";
    });
  }
  changePage(page: Paged) {
    this.filter.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.filter.take = take;
    this.findData();
  }
  detailPasien(model: RegistrasiPelayananPasienModel) {
    this.router.navigate(['/home/registrasi-pasien', { id: model.pasien.id }]);
  }

}
