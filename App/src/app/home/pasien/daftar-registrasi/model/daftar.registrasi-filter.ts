

import { JenisLampiran } from "app/models/registrasi.pelayanan.lampiran";
import { Ruangan } from 'app/models/ruangan';
import { Dokter } from 'app/models/dokter';

export class Criteria {
    public page: number;
    public take: number;
    public keyword:string;
    constructor() {

    }
}
export class JadwalDokterFilter extends Criteria{
    public dariTanggal: Date;
    public sampaiTanggal: Date;
    public dokterId:string;
    public dokter:Dokter;
    public ruanganId:string;
    public ruangan:Ruangan;
    constructor()
    {
        super();        
    }
}

export class RegistrasiObatFilter extends Criteria{
    public dariTanggal: Date;
    public sampaiTanggal: Date;
    public registrationId:string;
}

export class LampiranRegistrasiFilter extends Criteria{
    public dariTanggal: Date;
    public sampaiTanggal: Date;
    public registrationId:string;
    public jenisLampiran:JenisLampiran;
}
export class DaftarCatatanKlinisFilter extends Criteria{
    public registrationId:string;
}
export class DaftarDiagnosaFilter extends Criteria{
    public registrationId:string;
}
export class DaftarEcgFilter extends Criteria{
    public registrationId:string;
}
export class DaftarRegistrasiFilter extends Criteria {
    public dariTanggal: Date;
    public sampaiTanggal: Date;
    public noCm: string;
    public namaPasien: string;
    constructor() {
        super();
    }
}

export class DaftarPasienFilter extends Criteria {
    public dariTanggal: Date;
    public sampaiTanggal: Date;
    public noCm: string;
    public namaPasien: string;
    constructor() {
        super();
    }
}