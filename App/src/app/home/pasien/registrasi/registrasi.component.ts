import { Pasien } from 'app/models/pasien';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { AppSettings } from 'app/helper/appsetting';
import { WizardItem } from 'app/components/wizard/models/wizard.item';
import { Component, OnInit, HostBinding } from '@angular/core';
import { slideInDownAnimation } from "app/animations/slideInDownAnimation";
import { CoreHttpService } from "app/services/core-http.service";
import { RegistrasiPasienModel } from 'app/models/registrasi.pasien';
import { Alamat } from 'app/models/alamat';
import { PatientService } from "app/services/patient.service";
import { PushNotificationService } from "app/services/push-notification.service";
import { environment } from "environments/environment";
import { MessageType } from "app/models/messageType";
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'phr-registrasi',
  templateUrl: './registrasi.component.html',
  providers: [PatientService, PushNotificationService],
  styleUrls: ['./registrasi.component.scss'],
  animations: [slideInDownAnimation]
})
export class RegistrasiComponent implements OnInit {
  currentModel: RegistrasiPasienModel;
  isLoading: boolean;
  isSuccess: boolean;
  message: MessageType;
  @HostBinding('@routeAnimation') routeAnimation = true;
  @HostBinding('style.display') display = 'block';
  constructor(private route: ActivatedRoute, private coreHttp: CoreHttpService, private patientService: PatientService, private notification: PushNotificationService, private router: Router) {
    let alamat = new Alamat();
    let pasien = new Pasien();
    this.currentModel = new RegistrasiPasienModel();
    this.currentModel.pasien = pasien;
    this.currentModel.alamat = alamat;
    this.isLoading = false;
    this.message = new MessageType();
  }
  items: Array<WizardItem>;
  addDetailAlamat: boolean;
  pendidikanMetaData: InputTextItem;
  statusPernikahanMetaData: InputTextItem;
  titlePasienMetaData: InputTextItem;
  noBpjsMetaData: InputTextItem;
  namaKeluargaMetaData: InputTextItem;
  propinsiMetaData: InputTextItem;
  pekerjaanMetaData: InputTextItem;
  namaPanggilanMetaData: InputTextItem;
  namaPasienMetaData: InputTextItem;
  tanggalLahirMetaData: InputTextItem;
  tempatLahirMetaData: InputTextItem;
  jenisKelaminMetaData: InputTextItem;
  agamaMetaData: InputTextItem;
  noIdentitasMetaData: InputTextItem;
  alamatMetaData: InputTextItem;
  rtRwMetaData: InputTextItem;
  kotaMetaData: InputTextItem;
  kecamatanMetaData: InputTextItem;
  kelurahanMetaData: InputTextItem;
  kodePosMetaData: InputTextItem;
  toogleDetailAlamat() {
    this.addDetailAlamat = !this.addDetailAlamat;
  }
  ngOnInit() {
    this.route.params.subscribe(n => {
      if (n.id !== undefined)
        this.coreHttp.Get(environment.PATIENT_ENDPOINT + "Pasien/" + n.id).subscribe(pasien => {
          if (pasien !== null)
            this.currentModel.pasien = pasien;
        });
    });
    //  this.currentModel= new RegistrasiPasienModel(null.null);
    let wizard = AppSettings.WIZARD_REGISTRATION;
    wizard[0].Active();
    this.items = wizard;
    this.namaPasienMetaData = new InputTextItem("Nama Pasien", "Isi dengan Nama Pasien, contoh : 'Dimas Panji Indarto'", "string");
    this.namaPasienMetaData.required = true;

    this.noBpjsMetaData = new InputTextItem("No BPJS", "Isi dengan No Bpjs", "string");
    this.noBpjsMetaData.mask = "000000000000000000";
    this.namaPanggilanMetaData = new InputTextItem("Nama Panggilan", "Isi dengan Nama Panggilan", "string");
    this.namaKeluargaMetaData = new InputTextItem("Nama Keluarga", "Isi dengan Nama Keluarga", "string");
    this.tempatLahirMetaData = new InputTextItem("Tempat Lahir", "Isi dengan Tempat Lahir", "string");
    this.tempatLahirMetaData.required = true;
    this.alamatMetaData = new InputTextItem("Alamat Pasien", "Isi dengan Alamat pasien", "string");
    this.alamatMetaData.required = true;
    this.rtRwMetaData = new InputTextItem("RT/RW", "", "string");
    this.rtRwMetaData.mask = "00/00";

    this.kodePosMetaData = new InputTextItem("Kode pos", "Isi dengan kode pos tempat tinggal pasien", "string");
    this.kodePosMetaData.mask = "00000";


    this.noIdentitasMetaData = new InputTextItem("No. KTP", "Isi dengan No Identitas", "string");
    this.noIdentitasMetaData.required = true;
    this.tanggalLahirMetaData = new InputTextItem("Tanggal Lahir", "Isi dengan tanggal lahir pasien dd-MM-yyyy", "datetime");
    this.tanggalLahirMetaData.required = true;
    this.jenisKelaminMetaData = new InputTextItem("Jenis Kelamin", "Pilihlah jenis kelamin untuk pasien", "select");
    this.jenisKelaminMetaData.required = true;
    this.jenisKelaminMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "JenisKelamin/Search",
      fields: { title: 'namaJenisKelamin', description: 'Jenis Kelamin' },
      searchFullText: true
    };


    this.agamaMetaData = new InputTextItem("Agama", "Pilihlah agama untuk pasien", "select");
    this.agamaMetaData.required = true;
    this.agamaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Agama/Search",
      fields: { title: 'namaAgama', description: 'Agama' },
      searchFullText: true
    };
    this.propinsiMetaData = new InputTextItem("Propinsi", "Isi dengan propinsi tempat tinggal pasien", "select");

    this.propinsiMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Propinsi/Search",
      fields: { title: 'namaPropinsi', description: 'Propinsi' },
      searchFullText: true
    };

    this.pekerjaanMetaData = new InputTextItem("Pekerjaan", "Isi dengan pekerjaan tempat tinggal pasien", "select");
    this.pekerjaanMetaData.required = true;
    this.pekerjaanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Pekerjaan/Search",
      fields: { title: 'namaPekerjaan', description: 'Pekerjaan' },
      searchFullText: true
    };

    this.pendidikanMetaData = new InputTextItem("Pendidikan", "Isi dengan pendidikan tempat tinggal pasien", "select");
    this.pendidikanMetaData.required = true;
    this.pendidikanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Pendidikan/Search",
      fields: { title: 'namaPendidikan', description: 'Pendidikan' },
      searchFullText: true
    };

    this.statusPernikahanMetaData = new InputTextItem("Status Pernikahan", "Isi dengan status pernikahan tempat tinggal pasien", "select");
    this.statusPernikahanMetaData.required = true;
    this.statusPernikahanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "StatusPernikahan/Search",
      fields: { title: 'namaStatusPernikahan', description: 'StatusPernikahan' },
      searchFullText: true
    };
    this.titlePasienMetaData = new InputTextItem("Title Pasien", "Isi dengan status title pasien tinggal pasien", "select");
    this.titlePasienMetaData.required = true;
    this.titlePasienMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "TitlePasien/Search",
      fields: { title: 'namaTitlePasien', description: 'TitlePasien' },
      searchFullText: true
    };
    this.kotaMetaData = new InputTextItem("Kota", "Isi dengan kota tempat tinggal pasien", "select");
    this.kotaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kota/Search",
      fields: { title: 'namaKota', description: 'Kota' },
      searchFullText: true
    };
    this.kecamatanMetaData = new InputTextItem("Kecamatan", "Isi dengan Kecamatan tempat tinggal pasien", "select");
    this.kecamatanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kecamatan/Search",
      fields: { title: 'namaKecamatan', description: 'Kecamatan' },
      searchFullText: true
    };
    this.kelurahanMetaData = new InputTextItem("Kelurahan", "Isi dengan Kelurahan tempat tinggal pasien", "select");
    this.kelurahanMetaData.required = true;
    this.kelurahanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kelurahan/Search",
      fields: { title: 'namaKelurahan', description: 'Kelurahan' },
      searchFullText: true
    };
  }
  propinsiSelected(event: any): void {
    this.kotaMetaData = new InputTextItem("Kota di " + event.namaPropinsi, "Isi dengan Kota tempat tinggal pasien", "select");
    this.kotaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kota/" + event.id + "/Search",
      fields: { title: 'namaKota', description: 'Kota' },
      searchFullText: true
    };
  }
  kotaSelected(event: any): void {
    this.kecamatanMetaData = new InputTextItem("Kecamatan di " + event.namaKota, "Isi dengan Kecamatan tempat tinggal pasien", "select");
    this.kecamatanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kecamatan/" + event.id + "/Search",
      fields: { title: 'namaKecamatan', description: 'Kecamatan' },
      searchFullText: true
    };
  }

  kecamatanSelected(e: any): void {
    this.kelurahanMetaData = new InputTextItem("Kelurahan di " + e.namaKecamatan, "Isi dengan Kelurahan tempat tinggal pasien", "select");
    this.kelurahanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kelurahan/" + e.id + "/Search",
      fields: { title: 'namaKelurahan', description: 'Kelurahan' },
      searchFullText: true
    };
  }
  kelurahanSelected(e: any): void {
    this.currentModel.alamat.kodePos = e.kodePos;
    if (this.currentModel.alamat.propinsi === undefined) {
      this.currentModel.alamat.kecamatan = e.kecamatan;
      this.currentModel.alamat.kota = e.kecamatan.kota;
      this.currentModel.alamat.propinsi = e.kecamatan.kota.propinsi;

    }

  }
  nextToRegistrasiPelayanan(): void {
    this.router.navigate(['/home/registrasi-pelayanans/' + this.currentModel.id]);
  }
  savePasien(): void {

    if (this.currentModel.IsValid()) {
      this.isLoading = true;
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.patientService.savePasien(this.currentModel).subscribe(
        result => {
          let self = this;

          if (result.statusCode === 200) {
            this.isSuccess = true;
            this.notification.receive.subscribe(function (e) {
              self.currentModel.pasien.noRekamMedis = e.NoRekamMedis;
              self.message.content = "No Rekam medis : " + e.NoRekamMedis;
              setTimeout(function () {
                self.message.type = undefined;
                self.notification.unsubscribe = result.data.id;
              }, 5000);
            });
            this.notification.subscribe = result.data.id;
            this.currentModel.id = result.data.id;

          }


          this.message.type = result.statusCode;
          this.message.content = result.message.replace("\n", ",");
          return result;

        },
        err => {
          this.isLoading = false;
          this.message.type = "404";
          this.message.content = "Terjadi kesalahan";
        });
    }
  }

}

