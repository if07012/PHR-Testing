import { PushNotificationService } from './../../services/push-notification.service';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'app/services/authentication';
import { UserModel } from 'app/models/agama';
declare var $: any;
declare var toastr: any;
@Component({
  selector: 'phr-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [PushNotificationService]
})

export class DashboardComponent implements OnInit, AfterViewInit {
  user: UserModel = new UserModel();
  ngAfterViewInit(): void {
    $("#menu").click(function () {
      $('.ui.sidebar')
        .sidebar('toggle')
    })
  }

  private message: any;
  constructor(private service: PushNotificationService, private routers: Router, private authenticationService: AuthenticationService) {

  }

  ngOnInit() {
    let self = this;
    this.authenticationService.GetUser().subscribe(n => {
      this.user = n.data;
      this.service.receive.subscribe(function (e) {
        toastr["success"](e)
      });
      this.service.subscribe = n.data.profile.id;
    });
  }
  logout() {
    window.localStorage.removeItem('x-token');
    this.routers.navigate(['login']);
  }
  openSysadmin(){
    this.routers.navigate(["sysadmin"],{skipLocationChange: true});
  }

}
