import { RegistrasiComponent } from './pasien/registrasi/registrasi.component';
import { AuthenticationResolver } from './../resolver/authentication.resolver';
// Imports
// Deprecated import
// import { RouterConfig } from '@angular/router';
import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';


// import { CatListComponent }    from './cat-list.component';
// import { CatDetailsComponent }    from './cat-details.component';
import { RegistrasiPelayananComponent } from './pasien/registrasi-pelayanan/registrasi-pelayanan.component';
import { DaftarRegistrasiComponent } from './pasien/daftar-registrasi/daftar-registrasi.component';
import { DaftarPelayananComponent } from './pelayanan/daftar/daftar.component';
import { DiagnosaComponent } from './pelayanan/diagnosa/diagnosa.component';
import { PageNotFoundComponent } from '../not-found.component';
import { DaftarDiagnosaComponent } from './pelayanan/diagnosa/daftar-diagnosa/daftar-diagnosa.component';
import { CatatanKlinisComponent } from './pelayanan/catatan-klinis/catatan-klinis.component';
import { DaftarCatatanKlinisComponent } from './pelayanan/catatan-klinis/daftar/daftar.component';
import { DaftarEcgComponent } from "app/home/pelayanan/ecg/daftar/daftar.component";
import { EcgComponent } from './pelayanan/ecg/ecg.component';
import { NewEcgComponent } from './pelayanan/ecg/new/new.component';
import {  DetailEcgComponent } from './pelayanan/ecg/detail/detail.component';
import { DaftarPasienComponent } from "app/home/pasien/daftar/daftar.component";
import { LampiranComponent } from './pelayanan/lampiran/lampiran.component';
import { DaftarLampiranComponent } from './pelayanan/lampiran/daftar/daftar.component';
import { InputObatComponent } from './pelayanan/obat/obat.component';
import { DaftarObatComponent } from "app/home/pelayanan/obat/daftar/daftar.component";


// Route Configuration
export const homeRoutes: Routes = [
  {
    path: 'home', component: DashboardComponent, resolve: {
      transaction: AuthenticationResolver
    }, children: [
      {
        path: 'registrasi-pasien', component: RegistrasiComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'registrasi-pasien/:id', component: RegistrasiComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'registrasi-pelayanan', component: RegistrasiPelayananComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'registrasi-pelayanans/:id', component: RegistrasiPelayananComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'daftar/registrasi-pelayanan', component: DaftarRegistrasiComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'daftar/pasien', component: DaftarPasienComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'pelayanan/:id', component: DaftarPelayananComponent, resolve: {
          transaction: AuthenticationResolver
        }, children:
        [
          {
            path: 'input/diagnosa/:id',
            component: DiagnosaComponent
          },
          {
            path: 'input/diagnosa',
            component: DiagnosaComponent
          },
          {
            path: 'daftar/diagnosa',
            component: DaftarDiagnosaComponent
          },

          {
            path: 'input/catatan-klinis/:id',
            component: CatatanKlinisComponent
          },
          {
            path: 'input/catatan-klinis',
            component: CatatanKlinisComponent
          },
          {
            path: 'daftar/catatan-klinis',
            component: DaftarCatatanKlinisComponent
          },
          {
            path: 'daftar/ecg',
            component: DaftarEcgComponent
          },
          {
            path: 'input/ecg',
            component: EcgComponent
          }
          ,{
            path: 'edit/ecg/:id',
            component: EcgComponent
          }
          ,{
            path: 'new/ecg',
            component: NewEcgComponent
          }
          ,{
            path: 'detail/ecg/:id',
            component: DetailEcgComponent
          },{
            path: 'lampiran/new',
            component: LampiranComponent
          },{
            path: 'lampiran/daftar',
            component: DaftarLampiranComponent
          }
          ,{
            path: 'obat/new',
            component: InputObatComponent
          },{
            path: 'obat/daftar',
            component: DaftarObatComponent
          },
          {
            path: 'obat/edit/:id',
            component: InputObatComponent
          }
        ]
      }
    ]
  },

  //   { path: 'cats/:id', component: CatDetailsComponent }
];