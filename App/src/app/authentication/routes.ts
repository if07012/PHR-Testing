// Imports
// Deprecated import
// import { RouterConfig } from '@angular/router';
import { Routes } from '@angular/router';
import { SigninComponent } from './signin/signin.component';

// import { CatListComponent }    from './cat-list.component';
// import { CatDetailsComponent }    from './cat-details.component';


// Route Configuration
export const authenticationRoutes: Routes = [
  { path: 'login', component: SigninComponent },
  //   { path: 'cats/:id', component: CatDetailsComponent }
];