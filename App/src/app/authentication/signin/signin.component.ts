import { Router } from '@angular/router';
import { AuthenticationService } from './../../services/authentication';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { LoginModel } from '../../models/login';

import { Observable } from 'rxjs/Rx';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';

@Component({
  selector: 'phr-signin',
  templateUrl: './signin.component.html',
  providers: [AuthenticationService],
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(private service: AuthenticationService, private routers: Router) {
    this.passwordMetaData = new InputTextItem("Password", "", "password");
    this.passwordMetaData.id="sign_in_password";
    this.userNameMetaData = new InputTextItem("User name", "", "string");
    this.userNameMetaData.id="sign_in_username";
  }
  userNameMetaData: InputTextItem;
  passwordMetaData: InputTextItem;
  ngOnInit() {
    this.model = new LoginModel("", "");
    if (!this.service.IsLogin())
      this.isLogin = false;
    else {
      this.isLogin = true;
      this.message = "Pemakain sudah login";
      let self = this;
      setTimeout(function () {
        self.routers.navigate(['home']);
      }, 1000);
    }
  }

  @Input() model: LoginModel;
  isLogin: Boolean
  isLoading: Boolean;
  message: String
  detail: String
  batal() {
    this.model = new LoginModel("", "");
  }
  login() {
    this.isLoading = true;
    this.message = "Verifikasi";
    this.detail = "Harap Tunggu ....";
    let result = this.service.Authentication(this.model).subscribe(
      result => {
        this.isLoading = false;
        if (result.statusCode === 200) {
          window.localStorage.setItem('x-token', result.data.token);
          this.isLogin = true;
          this.message = result.data.message;
          let self = this;
          setTimeout(function () {
            self.routers.navigate(['home']);
          }, 1000);
        } else {
          this.isLoading = true;
          this.message = "Gagal Login";
          this.detail = result.message;
        }

      },
      err => {
        this.isLoading = true;
        this.message = "Gagal Login";
        this.detail = "Terjadi Kesalahan";

      });;
  }

}
