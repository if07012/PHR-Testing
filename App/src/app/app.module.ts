import { AuthenticationService } from './services/authentication';
import { CoreHttpService } from './services/core-http.service';

import { AuthenticationResolver } from './resolver/authentication.resolver';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';
import { DashboardComponent } from './home/dashboard/dashboard.component';
import { SysAdminDashboardComponent } from './sysadmin/dashboard/dashboard.component';
import { routing } from './app.routes';
import { SigninComponent } from './authentication/signin/signin.component';
import { RegistrasiComponent } from './home/pasien/registrasi/registrasi.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { WizardComponent } from './components/wizard/wizard.component';
import { InputTextComponent } from './components/input-text/input-text.component';
import { PatientService } from 'app/services/patient.service';
import { LoadingMessageComponent } from './components/loading-message/loading-message.component';
import { RegistrasiPelayananComponent } from './home/pasien/registrasi-pelayanan/registrasi-pelayanan.component';
import { DaftarRegistrasiComponent } from './home/pasien/daftar-registrasi/daftar-registrasi.component';
import { ButtonComboComponent } from './components/button-combo/button-combo.component';
import { PagerComponent } from './components/pager/pager.component';
import { DaftarPelayananComponent } from './home/pelayanan/daftar/daftar.component';
import { DiagnosaComponent } from './home/pelayanan/diagnosa/diagnosa.component';
import { PageNotFoundComponent } from './not-found.component';
import { DaftarDiagnosaComponent } from './home/pelayanan/diagnosa/daftar-diagnosa/daftar-diagnosa.component';
import { CatatanKlinisComponent } from './home/pelayanan/catatan-klinis/catatan-klinis.component';
import { DaftarCatatanKlinisComponent } from './home/pelayanan/catatan-klinis/daftar/daftar.component';
import { DaftarEcgComponent } from './home/pelayanan/ecg/daftar/daftar.component';
import { EcgComponent } from './home/pelayanan/ecg/ecg.component';
import { PopUpDirective } from './components/popup/pop-up.directive';
import { NewEcgComponent } from './home/pelayanan/ecg/new/new.component';
import { DetailEcgComponent } from './home/pelayanan/ecg/detail/detail.component';
import { ChartEcgComponent } from './home/pelayanan/ecg/chart/chart.component';
import { DaftarPasienComponent } from './home/pasien/daftar/daftar.component';
import { JadwalDokterComponent } from './home/pasien/registrasi-pelayanan/jadwal-dokter/jadwal-dokter.component';
import { UserComponent } from './sysadmin/user/user.component';
import { SysadminService } from 'app/services/sysadmin.service';
import { JenisKelaminComponent } from './sysadmin/jenis-kelamin/jenis-kelamin.component';
import { AgamaComponent } from './sysadmin/agama/agama.component';
import { KotaComponent } from './sysadmin/kota/kota.component';
import { KecamatanComponent } from './sysadmin/kecamatan/kecamatan.component';
import { KelurahanComponent } from './sysadmin/kelurahan/kelurahan.component';
import { RuanganComponent } from './sysadmin/ruangan/ruangan.component';
import { JenisPelayananComponent } from './sysadmin/jenis-pelayanan/jenis-pelayanan.component';
import { DepartemenComponent } from './sysadmin/departemen/departemen.component';
import { SysadminDiagnosaComponent } from "app/sysadmin/diagnosa/diagnosa.component";
import { LampiranComponent } from './home/pelayanan/lampiran/lampiran.component';
import { DaftarLampiranComponent } from './home/pelayanan/lampiran/daftar/daftar.component';
import { InputObatComponent } from './home/pelayanan/obat/obat.component';
import { DaftarObatComponent } from './home/pelayanan/obat/daftar/daftar.component';
import { DokterComponent } from './sysadmin/dokter/dokter.component';
import { SysadminJadwalDokterComponent } from "app/sysadmin/jadwal-dokter/jadwal-dokter.component";

@NgModule({
  declarations: [
    AppComponent,
    WelcomePageComponent,
    DashboardComponent,
    SysAdminDashboardComponent,
    SigninComponent,
    RegistrasiComponent,
    WizardComponent,
    InputTextComponent,
    LoadingMessageComponent,
    RegistrasiPelayananComponent,
    DaftarRegistrasiComponent,
    ButtonComboComponent,
    PagerComponent,
    DaftarPelayananComponent,
    DiagnosaComponent,
    PageNotFoundComponent,
    DaftarDiagnosaComponent,
    CatatanKlinisComponent,
    DaftarCatatanKlinisComponent,
    DaftarEcgComponent,
    EcgComponent,
    PopUpDirective,
    NewEcgComponent,
    DetailEcgComponent,
    ChartEcgComponent,
    DaftarPasienComponent,
    JadwalDokterComponent,
    UserComponent,
    JenisKelaminComponent,
    AgamaComponent,
    KotaComponent,
    KecamatanComponent,
    KelurahanComponent,
    RuanganComponent,
    JenisPelayananComponent,
    DepartemenComponent,
    SysadminDiagnosaComponent,
    LampiranComponent,
    DaftarLampiranComponent,
    InputObatComponent,
    DaftarObatComponent,
    DokterComponent,
    SysadminJadwalDokterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing, BrowserAnimationsModule,    
  ],
  providers: [AuthenticationService, AuthenticationResolver, CoreHttpService, PatientService,SysadminService],
  bootstrap: [AppComponent]
})
export class AppModule { }
