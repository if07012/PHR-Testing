import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';


import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { LoginModel } from "app/models/login";
import { LoginResult } from "app/models/login.result";
import { environment } from 'environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(private http: Http) { }
  // private instance variable to hold base url
  GetUser(): Observable<any> {
    let bodyString = JSON.stringify(""); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json','phr-token':this.Token() }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option
    let resultModel = new LoginResult("", "", "");
    return this.http.post(environment.AUTHENTICATION_ENDPOINT+"User/CurrentUser", bodyString, options)
      .map((res: Response) => res)
      .catch(err => Observable.throw(err))
      .map((res) => res.json());


  }
  Authentication(model: LoginModel): Observable<any> {
    let bodyString = JSON.stringify(model); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
    let options = new RequestOptions({ headers: headers }); // Create a request option
    let resultModel = new LoginResult("", "", "");
    return this.http.post(environment.AUTHENTICATION_ENDPOINT+"auth", bodyString, options)
      .map((res: Response) => res)
      .catch(err => Observable.throw(err))
      .map((res) => res.json());


  }

  IsLogin(): Boolean {
    return !(window.localStorage.getItem("x-token") === null || window.localStorage.getItem("x-token") === undefined);
  }

   Token(): string {
    return window.localStorage.getItem("x-token");
  }
}
