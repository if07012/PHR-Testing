import { Injectable } from '@angular/core';
import { CoreHttpService } from 'app/services/core-http.service';
import { RegistrasiPasienModel } from 'app/models/registrasi.pasien';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Rx';
import { RegistrasiPelayananPasienModel, Result, ResultPaged, BufferEcgModel } from 'app/models/registrasi.pelayanan.pasien';
import { RegistrasiPelayananDiagnosaModel, RegistrasiPelayananEcgModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { DaftarRegistrasiFilter, DaftarDiagnosaFilter, DaftarPasienFilter, LampiranRegistrasiFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { RegistrasiPelayananCatatanKlinisModel } from 'app/models/registrasi.pelayanan.catatan.klinis';
import { DaftarCatatanKlinisFilter, DaftarEcgFilter, RegistrasiObatFilter } from '../home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import {  MappingBufferEcg, DetailEcg } from '../models/registrasi.pelayanan.pasien';
import { NewEcgFilter } from "app/home/pelayanan/ecg/new/model/new.ecg.filter";
import { RegistrasiPelayananLampiranModel } from 'app/models/registrasi.pelayanan.lampiran';
import { RegistrasiPelayananObatModel } from 'app/models/registrasi.pelayanan.obat';

@Injectable()
export class PatientService {

  constructor(private http: CoreHttpService
  ) { }
  savePasien(model: RegistrasiPasienModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "Pasien", model);

  }
  updateStatusRegistrasiPelayanan(model: RegistrasiPelayananPasienModel): Observable<any> {
    return this.http.Put(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/UpdateStatus", model);
  }
  saveRegistrasiPelayanan(model: RegistrasiPelayananPasienModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan", model);
  }
  saveLampiranPelayanan(model: RegistrasiPelayananLampiranModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Lampiran", model);
  }
  saveDiagnosa(model: RegistrasiPelayananDiagnosaModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Diagnosa", model);
  }

  getPagedRegistrasiPelayanan(model: DaftarRegistrasiFilter): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/PagedList", model);

  }
  getPagedPasien(model: DaftarPasienFilter): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "Pasien/PagedList", model);

  }
  getDetailObat(id: string): Observable<Result<RegistrasiPelayananObatModel>> {
    return this.http.Get(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Obat/ById/" + id);
  }
  getRegistrasiPelayanan(id: string): Observable<Result<RegistrasiPelayananPasienModel>> {
    return this.http.Get(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/ById/" + id);
  }
  getEcgById(id: string): Observable<Result<RegistrasiPelayananEcgModel>> {
    return this.http.Get(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Ecg/ById/" + id);
  }

  sendNotifEcgById(id: string): Observable<Result<RegistrasiPelayananEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Ecg/SendMessage", id);
  }
  getNewEcg(filter: NewEcgFilter): Observable<ResultPaged<BufferEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/NewEcg", filter);
  }
  countNewEcg(filter: NewEcgFilter): Observable<ResultPaged<BufferEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/NewEcg/Count", filter);
  }
  getPagedRegistrasiPelayananDiagnosa(filter: DaftarDiagnosaFilter): Observable<ResultPaged<RegistrasiPelayananDiagnosaModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Diagnosa/PagedList", filter);
  }
  getPagedRegistrasiPelayananEcg(filter: DaftarEcgFilter): Observable<ResultPaged<RegistrasiPelayananEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Ecg/PagedList", filter);
  }

  saveCatatanKlinis(model: RegistrasiPelayananCatatanKlinisModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/CatatanKlinis", model);
  }
  saveObat(model: RegistrasiPelayananObatModel): Observable<any> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Obat", model);
  }
  getPagedRegistrasiPelayananCatatanKlinis(filter: DaftarCatatanKlinisFilter): Observable<ResultPaged<RegistrasiPelayananCatatanKlinisModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/CatatanKlinis/PagedList", filter);
  }
  getPagedLampiranRegistrasi(filter: LampiranRegistrasiFilter): Observable<ResultPaged<RegistrasiPelayananLampiranModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Lampiran/PagedList", filter);
  }
  getPagedObatRegistrasi(filter: RegistrasiObatFilter): Observable<ResultPaged<RegistrasiPelayananObatModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Obat/PagedList", filter);
  }
  saveEcg(model: RegistrasiPelayananEcgModel): Observable<Result<RegistrasiPelayananEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Ecg", model);
  }
  mappingEcg(model: MappingBufferEcg): Observable<Result<BufferEcgModel>> {
    return this.http.Post(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/Ecg/Mapping", model);
  }
  detailEcg(id: string): Observable<Result<DetailEcg>> {
    return this.http.Get(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/DetailEcg/" + id);
  }

  updateEcgDetail(model: BufferEcgModel): Observable<Result<BufferEcgModel>> {
    return this.http.Put(environment.PATIENT_ENDPOINT + "RegistrasiPelayanan/DetailEcg/", model);
  }


}
