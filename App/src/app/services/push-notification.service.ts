import { Injectable, Output, EventEmitter, OnDestroy, OnInit } from '@angular/core';
import * as io from 'socket.io-client';
import { environment } from '../../environments/environment';

@Injectable()
export class PushNotificationService implements OnInit, OnDestroy {
  ngOnDestroy(): void {

  }
  ngOnInit(): void {

  }

  private url = environment.MESSAGING_ENDPOINT;
  private id: string;
  @Output()
  receive: EventEmitter<any> = new EventEmitter<any>();
  private socket;
  set subscribe(val) {
    this.id = val;
    this.socket = io(this.url);
    let self = this;
    this.socket.on(val, (data) => {
      self.receive.emit(data);
    });
  }
  set unsubscribe(val) {
    if (this.socket !== undefined && val !== undefined) {
      this.socket.ondisconnect();
      this.socket.removeAllListeners("publish");
      this.socket.removeAllListeners(val);
    }
  }
  send(room: string, model: any) {
    model.room = room;
    this.socket.emit('publish', model);
  }
  constructor() {

  }

}
