import { AuthenticationService } from './authentication';
import { Observable } from 'rxjs/Rx';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CoreHttpService {

  constructor(private http: Http) { }
  Token(): string {
    return window.localStorage.getItem("x-token");
  }
  Post(url: string, body: any): Observable<any> {
    let bodyString =  (body); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('phr-token', this.Token());
    let options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.post(url, bodyString, options).map((res: Response) => res)
      .catch(err => Observable.throw(err))
      .map((res) => res.json());


  }
  Put(url: string, body: any): Observable<any> {

    let bodyString = JSON.stringify(body); // Stringify payload
    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('phr-token', this.Token());
    let options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.put(url, bodyString, options).map((res: Response) => res)
      .catch(err => Observable.throw(err))
      .map((res) => res.json());


  }

  Delete(url: string): Observable<any> {

    let headers = new Headers({ 'Content-Type': 'application/json' });
    headers.append('phr-token', this.Token());
    let options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.delete(url, options).map((res: Response) => res)
      .catch(err => Observable.throw(err))
      .map((res) => res.json());


  }

  Get(url: string): Observable<any> {
    var token = this.Token();
    let headers = new Headers({
      'phr-token': token,
      "content-type": 'application/json'
    });
    let options = new RequestOptions({ headers: headers }); // Create a request option
    return this.http.get(url, options)
      .map((res: Response) => {
        return res;
      })
      .catch(err => Observable.throw(err))
      .map((res) => res.json());

  }
}
