import { Injectable } from '@angular/core';
import { CoreHttpService } from 'app/services/core-http.service';
import { Observable } from 'rxjs/Observable';
import { ResultPaged } from 'app/models/registrasi.pelayanan.pasien';
import { UserModel } from '../models/agama';
import { environment } from 'environments/environment';
import { Criteria, JadwalDokterFilter } from '../home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { JadwalDokterComponent } from '../home/pasien/registrasi-pelayanan/jadwal-dokter/jadwal-dokter.component';
import { JenisKelamin } from '../models/jenis.kelamin';
import { Dokter, JadwalDokter } from 'app/models/dokter';

@Injectable()
export class SysadminService {

  constructor(private http: CoreHttpService) { }

  getUser(q: Criteria): Observable<ResultPaged<UserModel>> {
    let model = new Criteria();
    model.page = q.page;
    model.take = q.take;
    if (q.keyword === undefined)
      model.keyword = '-';
    model.keyword = q.keyword;
    return this.http.Post(environment.AUTHENTICATION_ENDPOINT + "User/PagedList/", model);
  }
  saveUser(q: UserModel): Observable<UserModel> {
    return this.http.Post(environment.AUTHENTICATION_ENDPOINT + "User/", q);
  }
  deleteUser(q: UserModel): Observable<UserModel> {
    return this.http.Delete(environment.AUTHENTICATION_ENDPOINT + "User/" + q.id);
  }


  getJenisKelamin(q: Criteria): Observable<ResultPaged<JenisKelamin>> {
    let model = new Criteria();
    model.page = q.page;
    model.take = q.take;
    if (q.keyword === undefined)
      model.keyword = '-';
    model.keyword = q.keyword;
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "JenisKelamin/PagedList/", model);
  }
  getJadwalDokter(q: JadwalDokterFilter): Observable<ResultPaged<JadwalDokter>> {
    let model =q;
    model.page = q.page;
    model.take = q.take;
    if (q.keyword === undefined)
      model.keyword = '-';
    model.keyword = q.keyword;
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "Dokter/Jadwal/PagedList/", model);
  }

  deleteJadwalDokter(id: string): Observable<JadwalDokter> {
    return this.http.Delete(environment.SYSADMIN_ENDPOINT + "Dokter/Jadwal/" + id);
  }
  saveJenisKelamin(q: JenisKelamin): Observable<JenisKelamin> {
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "JenisKelamin/", q);
  }
  saveJadwalDokter(q: JadwalDokter): Observable<JadwalDokter> {
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "Dokter/Jadwal/Save", q);
  }
  saveDokter(q: Dokter): Observable<Dokter> {
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "Dokter/Save", q);
  }
  getDokter(q: Criteria): Observable<ResultPaged<Dokter>> {
    let model = new Criteria();
    model.page = q.page;
    model.take = q.take;
    if (q.keyword === undefined)
      model.keyword = '-';
    model.keyword = q.keyword;
    return this.http.Post(environment.SYSADMIN_ENDPOINT + "Dokter/CustomPagedList/", model);
  }
  deleteJenisKelamin(q: JenisKelamin): Observable<JenisKelamin> {
    return this.http.Delete(environment.SYSADMIN_ENDPOINT + "JenisKelamin/" + q.id);
  }

  get<T>(nameTable: string, q: Criteria): Observable<ResultPaged<T>> {
    let model = new Criteria();
    model.page = q.page;
    model.take = q.take;
    if (model.page === undefined)
      model.page = 1;
    if (model.take === undefined)
      model.take = 10;
    if (q.keyword === undefined)
      model.keyword = '-';
    model.keyword = q.keyword;
    return this.http.Post(environment.SYSADMIN_ENDPOINT + nameTable + "/PagedList/", model);
  }
  save<T>(nameTable: string, q: T): Observable<T> {
    return this.http.Post(environment.SYSADMIN_ENDPOINT + nameTable + "/", q);
  }
  delete<T, TKey>(nameTable: string, id: TKey): Observable<T> {
    return this.http.Delete(environment.SYSADMIN_ENDPOINT + nameTable + "/" + id);
  }
}
