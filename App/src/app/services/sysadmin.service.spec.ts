import { TestBed, inject } from '@angular/core/testing';

import { SysadminService } from './sysadmin.service';

describe('SysadminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SysadminService]
    });
  });

  it('should ...', inject([SysadminService], (service: SysadminService) => {
    expect(service).toBeTruthy();
  }));
});
