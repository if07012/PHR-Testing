import { Component, OnInit, AfterViewInit, ElementRef, ViewChild } from '@angular/core';
import { environment } from 'environments/environment';
declare var $: any;
declare var moment: any;
declare var toastr:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],

})
export class AppComponent implements AfterViewInit {
  @ViewChild('selectElem') el: ElementRef;
  @ViewChild('autocomplete') al: ElementRef;
  ngAfterViewInit(): void {
    Date.prototype.toJSON = function () {
      return moment(this).format();
    }

     toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": true,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

  }


  title = 'Phr Front End';
}
