import { WizardItem } from './../components/wizard/models/wizard.item';
export class AppSettings {
   
   public static WIZARD_REGISTRATION = [
       new WizardItem("rp","Registrasi Pasien","Digunakan untuk melakukan registrasi pasien.",false,true),
       new WizardItem("rl","Registrasi Pelayanan","Digunakan untuk melakukan registrasi pelayanan",false,true),
       new WizardItem("p","Pelayanan","Digunakan untuk memberikan pelayanan yang akan di dapatakan oleh pasien",false,true)
   ]
}
