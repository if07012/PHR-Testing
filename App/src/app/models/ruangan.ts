import { BaseModel } from 'app/models/agama';
import { JenisPelayanan } from "app/models/kecamatan";



export class Departemen  extends BaseModel {
    public id: string;
    public namaDepartemen: string;
    public jenisPelayanan:JenisPelayanan;
    constructor()
    { 
        super();
    }
    Valid(): boolean {
        if (this.namaDepartemen === undefined) {
            this.message = "Nama Departemen tidak boleh kosong";
            return false;
        }      
        return true;
    }
}

export class Ruangan  extends BaseModel {
    public id: string;
    public namaRuangan: string;
    public departemen:Departemen;
    constructor()
    { 
        super();
    }
    Valid(): boolean {
        if (this.namaRuangan === undefined) {
            this.message = "Nama Ruangan tidak boleh kosong";
            return false;
        }      
        return true;
    }
}