import { JenisKelamin } from './jenis.kelamin';
import { Agama, BaseModel } from './agama';

export class StatusPernikahan extends BaseModel {
    public namaStatusPernikahan:string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaStatusPernikahan === undefined) {
            this.message = "Nama Status Pernikahan tidak boleh kosong";
            return false;
        }      
        return true;
    }
}
export class Pekerjaan extends BaseModel {
    public namaPekerjaan:string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaPekerjaan === undefined) {
            this.message = "Nama Pekerjaan tidak boleh kosong";
            return false;
        }      
        return true;
    }
}
export class Pendidikan extends BaseModel {
    public namaPendidikan:string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaPendidikan === undefined) {
            this.message = "Nama Pendidikan tidak boleh kosong";
            return false;
        }      
        return true;
    }
}
export class TitlePasien extends BaseModel {
    public namaTitlePasien:string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaTitlePasien === undefined) {
            this.message = "Nama Title Pasien tidak boleh kosong";
            return false;
        }      
        return true;
    }
}

export class Pasien {
    public id: string;
    public namaPasien: string;
    public tanggalLahir: Date;
    public tanggalRegistrasi: Date;
    public noRekamMedis: string;
    public agama: Agama;
    public jenisKelamin: JenisKelamin;
    public idJenisKelamin: string;
    public idAgama: string;
    public noIdentitas: string;

    public pendidikan: Pendidikan;
    public idPendidikan: string;
    public pekerjaan: Pekerjaan;
    public idPekerjaan: string;
    public statusPernikahan: StatusPernikahan;
    public idStatusPernikahan: string;
    public titlePasien: TitlePasien;
    public idTitlePasien: string;
    public noBpjs: string;
    public namaKeluarga: string;
    public namaPanggilan: string;
    public tempatLahir: string;
    constructor(

    ) {
    }
}


