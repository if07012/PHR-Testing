
import { ObatModel } from "app/models/obat.model";
import { JenisRacikanModel } from "app/models/jenis.racikan";
import { SignaModel } from "app/models/signa";
import { RegistrasiPelayananPasienModel } from "app/models/registrasi.pelayanan.pasien";
import { Pasien } from "app/models/pasien";
export class RegistrasiPelayananObatModel {
    public id: string;
    public noResep:string;
    public tanggalPelayanan: Date;
    public keterangan: string;
    public details: Array<RegistrasiPelayananObatDetailModel> = new Array<RegistrasiPelayananObatDetailModel>();
    public registrasiPasien: RegistrasiPelayananPasienModel;
    public pasien: Pasien;
    constructor() {
        this.tanggalPelayanan = new Date();
    }
    IsValid(): boolean {
        if (this.tanggalPelayanan === undefined) return false;
        return true;
    }
}
export class RegistrasiPelayananObatDetailModel {
    public id:string;
    public obat: ObatModel;
    public jenisRacikan: JenisRacikanModel;
    public jumlah: number;
    public tanggalPelayanan: Date;
    public keterangan: string;
    public signa: SignaModel;
    public registrasiPasien: RegistrasiPelayananPasienModel;
    public pasien: Pasien;
    constructor() {
        this.tanggalPelayanan = new Date();
    }
    IsValid(): boolean {
        if (this.tanggalPelayanan === undefined) return false;
        if (this.jenisRacikan === undefined) return false;
        if (this.obat === undefined) return false;
        return true;
    }
}