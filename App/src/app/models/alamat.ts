import { Kelurahan } from './kelurahan';
import { Kecamatan } from './kecamatan';
import { Kota } from './kota';
import { BaseModel } from './agama';

export class Propinsi extends BaseModel {
    public namaPropinsi: string;
    constructor()
    { super(); }
    Valid(): boolean {
        if (this.namaPropinsi === undefined) {
            this.message = "Nama Propinsi tidak boleh kosong";
            return false;
        }
        return true;
    }
}

export class Alamat {
    public alamatRumah: string;
    public kodePos: string;
    public kota: Kota;
    public idKota: string;

    public idPropinsi: string;
    public propinsi: Propinsi;
    public kecamatan: Kecamatan;
    public idKecamatan: string;
    public kelurahan: Kelurahan;
    public idKelurahan: string;
    public rtRw: string;
    constructor(

    ) { }
}


