import { Kota } from './kota';
import { BaseModel } from './agama';

export class Kecamatan extends BaseModel {
    public id: string;
    public namaKecamatan: string;
    public kota: Kota
    constructor()
    { super() }

    Valid(): boolean {
        if (this.namaKecamatan === undefined) {
            this.message = "Nama Kecamatan tidak boleh kosong";
            return false;
        }
        if (this.kota === undefined) {
            this.message = "Kota tidak boleh kosong";
            return false;
        }
        if (this.kota.id === undefined) {
            this.message = "Kota tidak boleh kosong";
            return false;
        }
        return true;
    }
}

export class JenisPelayanan extends BaseModel {
    public id: string;
    public namaJenisPelayanan: string;
    constructor()
    { super() }

    Valid(): boolean {
        if (this.namaJenisPelayanan === undefined) {
            this.message = "Nama Jenis palayanan tidak boleh kosong";
            return false;
        }
        return true;
    }
}