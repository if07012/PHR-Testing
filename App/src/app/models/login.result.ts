export class LoginResult {
    constructor(
        public statusCode: string, 
        public message: string, 
        public token:string
        ){}
}