
export class BaseModel {
    public id: string;
    public message: string;
}

export class Agama  extends BaseModel {
    public id: string;
    public namaAgama: string;
    constructor()
    { 
        super();
    }
    Valid(): boolean {
        if (this.namaAgama === undefined) {
            this.message = "Nama Agama tidak boleh kosong";
            return false;
        }      
        return true;
    }
}
export class JenisUserModel {
    public id: string;
    public namaJenisUser: string;
    constructor() {

    }
}
export class UserModel extends BaseModel {

    Valid(): boolean {
        if (this.username === undefined) {
            this.message = "User name tidak boleh kosong";
            return false;
        }
        if (this.jenisUser === undefined) {
            this.message = "jenis user tidak boleh kosong";
            return false;
        }
        if (this.password === undefined) {
            this.message = "password tidak boleh kosong";
            return false;
        }
        if (this.confirmPassword === undefined) {
            this.message = "Konfirm password tidak boleh kosong";
            return false;
        }
        if (this.confirmPassword !== this.password) {
            this.message = "Password tidak sesuai";
            return false;
        }
        return true;
    }
    public fullName:string;
    public id: string;
    public username: string;
    public password: string;
    public lastLogin: string;
    public jenisUser: JenisUserModel;
    public confirmPassword: string;
    constructor() {
        super();
        this.jenisUser = new JenisUserModel();
    }
}
