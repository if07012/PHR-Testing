export class BaseValidationModel {
    constructor() {

    }
    public message: string;
    public Valid(): boolean {
        return true;
    }
}
export class JenisKelamin extends BaseValidationModel {
    public id: string;
    public namaJenisKelamin: string;
    constructor() {
        super();
    }
    public Valid(): boolean {
        return this.namaJenisKelamin !== undefined;
    }
}