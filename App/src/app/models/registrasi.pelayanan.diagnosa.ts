import { Pasien } from './pasien';
import { Alamat } from './alamat';
import { Dokter } from "./dokter";
import { Ruangan } from './ruangan';
import { StatusPendaftaran } from './status-pendaftaran';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { BaseModel } from 'app/models/agama';
export class Result<T>{
    message: string;
    data: T;
    statusCode: number;
}
export class Diagnosa extends BaseModel {
    id: string;
    namaDiagnosa: string;
    kodeDiagnosa:string;
    constructor(){
        super();
    }
    Valid(): boolean {
        if (this.namaDiagnosa === undefined) {
            this.message = "Nama Diagnosa tidak boleh kosong";
            return false;
        }
        if (this.kodeDiagnosa === undefined) {
            this.message = "Kode Diagnosa tidak boleh kosong";
            return false;
        }
       
        return true;
    }
}
export class RegistrasiPelayananDiagnosaModel {
    errorMessages: string[];
    diagnosa: Diagnosa;
    dokter :Dokter;
    registrasiPasien: RegistrasiPelayananPasienModel;
    pasien: Pasien;
    keterangan:string;
    tanggalPelayanan: Date;
    constructor(
    ) {
    }
    IsValid(): boolean {
        if (this.tanggalPelayanan === undefined) return false;
        if (this.diagnosa === undefined) return false;

        return true;
    }
}
export class RegistrasiPelayananEcgModel {
    errorMessages: string[];
    id:string;
    diagnosa: string;
    dokter :Dokter;
    registrasiPasien: RegistrasiPelayananPasienModel;
    pasien: Pasien;
    instruksi:string;
    tanggalPelayanan: Date;
    constructor(
    ) {
    }
    IsValid(): boolean {
        if (this.tanggalPelayanan === undefined) return false;
        if (this.diagnosa === undefined) return false;

        return true;
    }
}
