import { Pasien } from './pasien';
import { Alamat } from './alamat';
import { Dokter } from "./dokter";
import { Ruangan } from './ruangan';
import { StatusPendaftaran } from './status-pendaftaran';
import { RegistrasiPelayananEcgModel } from 'app/models/registrasi.pelayanan.diagnosa';
import { MessageType } from 'app/models/messageType';
export class Result<T>{
    message: string;
    data: T;
    statusCode: number;
}

export class ResultPaged<T>{
    message: string;
    records: Array<T>;
    statusCode: number;
    totalRecords: number;
    take: number;
    currentPage: number;

}
export class DetailEcg {
    pelayanan: RegistrasiPelayananEcgModel;
    signals: Array<BufferEcgModel>;
    constructor() {
        this.pelayanan = new RegistrasiPelayananEcgModel();
        this.signals = new Array<BufferEcgModel>();
    }
}
export class MappingBufferEcg {
    registrasiModel: RegistrasiPelayananEcgModel;
    listBuffer: Array<BufferEcgModel>;
}
export class JenisSignal {
    id:string;
    namaSignal:string;
    namaJenisSignal: string;
    jenisSignalId: string;
}
export class BufferEcgModel {
    isLoading:boolean;
    id: String;
    isSelected: boolean;
    isHide: boolean;
    status: string;
    namaJenisSignal: string;
    jenisSignalId: string;
    message:MessageType;
    signal: JenisSignal;
    pasien: Pasien;
    noPonsel: string;
    fileName: string;
    decode:Array<string>
    email: string;
    tanggalPengiriman: string;
}
export class RegistrasiPelayananPasienModel {
    errorMessages: string[];
    actions: StatusPendaftaran[];
    idStatus: number;
    id: string;
    isLoading: boolean;
    pasien: Pasien;
    dokter: Dokter;
    statusPendaftaran: StatusPendaftaran;
    ruangan: Ruangan;
    tanggalRegistrasi: Date;
    constructor(
    ) {
        this.errorMessages = new Array<string>();
        this.pasien = new Pasien();
    }
    IsValid(): boolean {
        if (this.pasien.id === '') return false;
        if (this.dokter.id === '') return false;
        if (this.ruangan.id === '') return false;
        if (this.tanggalRegistrasi === undefined) return false;

        return true;
    }
}