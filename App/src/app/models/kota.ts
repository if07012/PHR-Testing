import { BaseModel } from './agama';
export class Kota extends BaseModel {
    public id: string;
    public namaKota: string;
    constructor()
    { super() }

    Valid(): boolean {
        if (this.namaKota === undefined) {
            this.message = "Nama Kota tidak boleh kosong";
            return false;
        }
        return true;
    }
}