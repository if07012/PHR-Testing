import { Pasien } from './pasien';
import { Alamat } from './alamat';


export interface IErrorMessages {
    errorMessages: Array<string>;
}
export class RegistrasiPasienModel implements IErrorMessages {
    id :string;
    errorMessages: string[];
    pasien: Pasien;
    alamat: Alamat;
    constructor(
    ) {
        this.errorMessages = new Array<string>();
    }
    IsValid(): boolean {
        if (this.pasien.namaPasien === '') return false;
        if (this.pasien.jenisKelamin === undefined || this.pasien.jenisKelamin.id === '') return false;
        if (this.pasien.tanggalLahir === undefined) return false;
        if (this.pasien.agama === undefined || this.pasien.agama.id === '') return false;

        if (this.pasien.titlePasien === undefined || this.pasien.titlePasien.id === '') return false;
        if (this.pasien.tempatLahir === undefined || this.pasien.tempatLahir === '') return false;
        if (this.pasien.pendidikan === undefined || this.pasien.pendidikan.id === '') return false;
        if (this.pasien.pekerjaan === undefined || this.pasien.pekerjaan.id === '') return false;
        if (this.pasien.statusPernikahan === undefined || this.pasien.statusPernikahan.id === '') return false;
        if (this.alamat.alamatRumah === '') return false;
        if (this.alamat.kelurahan === undefined || this.alamat.kelurahan.id === '') return false;
        return true;
    }
}