import { BaseModel } from 'app/models/agama';


export class ObatModel extends BaseModel {
    namaObat: string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaObat === undefined) {
            this.message = "Nama Obat tidak boleh kosong";
            return false;
        }
        return true;
    }
}