export class Paged {
   
    constructor( public page: number,
    public active: boolean)
    { }
}
