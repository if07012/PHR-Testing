import { BaseModel } from 'app/models/agama';

export class SignaModel extends BaseModel {
    namaSigna: string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaSigna === undefined) {
            this.message = "Nama Signa tidak boleh kosong";
            return false;
        }
        return true;
    }
}