import { Pasien } from './pasien';
import { Alamat } from './alamat';
import { Dokter } from "./dokter";
import { Ruangan } from './ruangan';
import { StatusPendaftaran } from './status-pendaftaran';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { BaseModel } from 'app/models/agama';

export class JenisLampiran extends BaseModel
{
    constructor(){super();}
    public namaJenisLampiran:string;
}
export class RegistrasiPelayananLampiranModel {
    link:string;
    id:string;
    errorMessages: string[];
    jenisLampiran: JenisLampiran;
    registrasiPasien: RegistrasiPelayananPasienModel;
    pasien: Pasien;
    tanggalUpload: Date;
    constructor(
    ) {
    }
    IsValid(): boolean {
        return true;
    }
}