import { BaseModel } from 'app/models/agama';

export class JenisRacikanModel extends BaseModel {
    namaJenisRacikan: string;
    constructor() {
        super();
    }
    Valid(): boolean {
        if (this.namaJenisRacikan === undefined) {
            this.message = "Nama Jenis Racikan tidak boleh kosong";
            return false;
        }
        return true;
    }
}