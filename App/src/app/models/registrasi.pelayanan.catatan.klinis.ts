import { Pasien } from './pasien';
import { Alamat } from './alamat';
import { Dokter } from "./dokter";
import { Ruangan } from './ruangan';
import { RegistrasiPelayananPasienModel } from 'app/models/registrasi.pelayanan.pasien';
import { BaseModel } from 'app/models/agama';

export class RegistrasiPelayananCatatanKlinisModel {
    errorMessages: string[];
    registrasiPasien: RegistrasiPelayananPasienModel;
    pasien: Pasien;
    sistolik: number;
    diastolik: number;
    keterangan: string;
    tanggalPelayanan: Date;
    beratBadan: number;
    tinggiBadan: number;
    suhu: number;
    tekanan: string;
    keluhan: string;
    constructor(
    ) {
    }
    IsValid(): boolean {
        if (this.tanggalPelayanan === undefined) return false;
        this.tekanan = this.sistolik + "/" + this.diastolik;
        return true;
    }
}