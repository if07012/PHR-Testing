import { BaseModel } from './agama';
import { Ruangan } from './ruangan';
export class JadwalDokter extends BaseModel{
    public tanggalMulaiJaga:string;
    public dokter:Dokter;
    public ruangan:Ruangan;
    Valid(): boolean {
        if (this.dokter === undefined) {
            this.message = "Nama Pegawai tidak boleh kosong";
            return false;
        }      
        return true;
    }
}
export class Dokter extends BaseModel {
    public id: string;
    public namaPegawai: string;
    public username: string;
    public idUser: string;
    public password: string;
    constructor()
    { super();}
    
    Valid(): boolean {
        if (this.namaPegawai === undefined) {
            this.message = "Nama Pegawai tidak boleh kosong";
            return false;
        }      
        return true;
    }
}