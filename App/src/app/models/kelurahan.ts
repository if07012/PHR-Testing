import { Kecamatan } from './kecamatan';
import { BaseModel } from "app/models/agama";

export class Kelurahan extends BaseModel {
    public id: string;
    public namaKelurahan: string;
    public kodePos: string;
    public kecamatan: Kecamatan;
    constructor(
    )
    { super(); }

     Valid(): boolean {
        if (this.namaKelurahan === undefined) {
            this.message = "Nama Kelurahan tidak boleh kosong";
            return false;
        }
        if (this.kecamatan === undefined) {
            this.message = "Kecamatan tidak boleh kosong";
            return false;
        }
        if (this.kecamatan.id === undefined) {
            this.message = "Kecamatan tidak boleh kosong";
            return false;
        }
        return true;
    }
}