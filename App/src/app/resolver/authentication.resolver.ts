import { AuthenticationService } from './../services/authentication';
import { Injectable, Component } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router'
import { Observable } from 'rxjs/Rx';

@Injectable()
export class AuthenticationResolver implements Resolve<any>
{
    constructor(private authenticationService: AuthenticationService,private routers :Router){

    }
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        //route.component.router.navigate('login');
        let isLogin= this.authenticationService.IsLogin();
        if(isLogin!=true)
            this.routers.navigate(['login']);
        return isLogin;
    }


}
