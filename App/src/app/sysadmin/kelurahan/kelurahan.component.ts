import { Component, OnInit } from '@angular/core';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { MessageType } from 'app/models/messageType';
import { Criteria } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { Paged } from 'app/models/paged';
import { SysadminService } from 'app/services/sysadmin.service';
import { environment } from 'environments/environment';
import { Kelurahan } from "app/models/kelurahan";
declare var $: any;
@Component({
  selector: 'phr-kelurahan',
  templateUrl: './kelurahan.component.html',
  styleUrls: ['./kelurahan.component.css']
})
export class KelurahanComponent implements OnInit {

  namaKelurahanMetaData: InputTextItem;
  kecamatanMetaData:InputTextItem;
  message: MessageType;
  search: Criteria;
  pages: Array<Paged>;
  list: Array<Kelurahan> = new Array<Kelurahan>();
  currentModel: Kelurahan;
  constructor(private service: SysadminService) {
    this.namaKelurahanMetaData = new InputTextItem("Kelurahan", "Isi dengan Kelurahan", "string");
    this.kecamatanMetaData = new InputTextItem("Kecamatan", "Pilihlah Kecamatan", "select");
    this.kecamatanMetaData.required = true;
    this.kecamatanMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kecamatan/Search",
      fields: { title: 'namaKecamatan', description: 'Kecamatan' },
      searchFullText: true
    };
    this.message = new MessageType();
    this.search = new Criteria();
    this.cancle();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.service.get<Kelurahan>("Kelurahan", this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.save<Kelurahan>("Kelurahan", this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new Kelurahan();
    this.message = new MessageType();
  }
  showDetail(item: Kelurahan) {
    this.currentModel = item;
  }
  delete(item: Kelurahan) {
    let self = this;
    this.currentModel = item;

    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          this.message = new MessageType();
          this.message.type = "loading";
          this.message.content = "Sedang proses....";
          self.service.delete<Kelurahan, string>("Kelurahan", item.id).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }

}
