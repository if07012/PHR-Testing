import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JenisPelayananComponent } from './jenis-pelayanan.component';

describe('JenisPelayananComponent', () => {
  let component: JenisPelayananComponent;
  let fixture: ComponentFixture<JenisPelayananComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JenisPelayananComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JenisPelayananComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
