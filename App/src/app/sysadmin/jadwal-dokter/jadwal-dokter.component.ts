import { Component, OnInit } from '@angular/core';
import { MessageType } from 'app/models/messageType';
import { Kecamatan } from 'app/models/kecamatan';
import { Paged } from 'app/models/paged';
import { Criteria, JadwalDokterFilter } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { environment } from 'environments/environment';
import { SysadminService } from 'app/services/sysadmin.service';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { JadwalDokter } from 'app/models/dokter';
declare var $: any;
@Component({
  selector: 'phr--sysadmin-jadwal-dokter',
  templateUrl: './jadwal-dokter.component.html',
  styleUrls: ['./jadwal-dokter.component.css']
})
export class SysadminJadwalDokterComponent implements OnInit {

  tanggalJadwalMetaData: InputTextItem;
  ruanganMetaData: InputTextItem;
  dokterMetaData: InputTextItem;
  message: MessageType;
  search: JadwalDokterFilter;
  pages: Array<Paged>;
  list: Array<JadwalDokter> = new Array<JadwalDokter>();
  currentModel: JadwalDokter;
  constructor(private service: SysadminService) {
    this.tanggalJadwalMetaData = new InputTextItem("Tanggal Jadwal", "Isi dengan tanggal", "datetime");
    this.dokterMetaData = new InputTextItem("Dokter", "Pilihlah Dokter", "select");
    this.dokterMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Dokter/Search",
      fields: { title: 'namaPegawai', description: 'Dokter' },
      searchFullText: true
    };

    this.ruanganMetaData = new InputTextItem("Ruangan", "Pilihlah Ruangan", "select");
    this.ruanganMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Ruangan/Search",
      fields: { title: 'namaRuangan', description: 'Ruangan' },
      searchFullText: true
    };
    this.message = new MessageType();
    this.search = new JadwalDokterFilter();
    this.cancle();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    if (this.search.ruangan !== undefined && this.search.ruangan != null)
      this.search.ruanganId = this.search.ruangan.id;
    if (this.search.dokter !== undefined && this.search.dokter != null)
      this.search.dokterId = this.search.dokter.id;
    this.service.getJadwalDokter(this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.saveJadwalDokter(this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new JadwalDokter();
    this.message = new MessageType();
  }
  showDetail(item: JadwalDokter) {
    this.currentModel = item;
  }
  delete(item: JadwalDokter) {
    let self = this;
    this.currentModel = item;

    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          this.message = new MessageType();
          this.message.type = "loading";
          this.message.content = "Sedang proses....";
          self.service.deleteJadwalDokter(item.id).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }

}
