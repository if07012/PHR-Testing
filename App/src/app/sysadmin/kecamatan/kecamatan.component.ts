import { Component, OnInit } from '@angular/core';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { MessageType } from 'app/models/messageType';
import { Criteria } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { Paged } from 'app/models/paged';
import { SysadminService } from 'app/services/sysadmin.service';
import { environment } from 'environments/environment';
import { Kecamatan } from "app/models/kecamatan";
declare var $: any;
@Component({
  selector: 'phr-kecamatan',
  templateUrl: './kecamatan.component.html',
  styleUrls: ['./kecamatan.component.css']
})
export class KecamatanComponent implements OnInit {

  namaKecamatanMetaData: InputTextItem;
  kotaMetaData:InputTextItem;
  message: MessageType;
  search: Criteria;
  pages: Array<Paged>;
  list: Array<Kecamatan> = new Array<Kecamatan>();
  currentModel: Kecamatan;
  constructor(private service: SysadminService) {
    this.namaKecamatanMetaData = new InputTextItem("Kecamatan", "Isi dengan Kecamatan", "string");
    this.kotaMetaData = new InputTextItem("Kota", "Pilihlah Kota", "select");
    this.kotaMetaData.required = true;
    this.kotaMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "Kota/Search",
      fields: { title: 'namaKota', description: 'Kota' },
      searchFullText: true
    };
    this.message = new MessageType();
    this.search = new Criteria();
    this.cancle();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.service.get<Kecamatan>("Kecamatan", this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.save<Kecamatan>("Kecamatan", this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new Kecamatan();
    this.message = new MessageType();
  }
  showDetail(item: Kecamatan) {
    this.currentModel = item;
  }
  delete(item: Kecamatan) {
    let self = this;
    this.currentModel = item;

    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          this.message = new MessageType();
          this.message.type = "loading";
          this.message.content = "Sedang proses....";
          self.service.delete<Kecamatan, string>("Kecamatan", item.id).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }

}
