import { Component, OnInit } from '@angular/core';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { MessageType } from 'app/models/messageType';
import { Criteria } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { Paged } from 'app/models/paged';
import { SysadminService } from 'app/services/sysadmin.service';
import { environment } from 'environments/environment';
import { JenisKelamin } from "app/models/jenis.kelamin";
declare var $: any;
@Component({
  selector: 'phr-jenis-kelamin',
  templateUrl: './jenis-kelamin.component.html',
  styleUrls: ['./jenis-kelamin.component.css']
})
export class JenisKelaminComponent implements OnInit {

  namaJenisKelaminMetaData: InputTextItem;
  message: MessageType;
  search: Criteria;
  pages: Array<Paged>;
  list: Array<JenisKelamin> = new Array<JenisKelamin>();
  currentModel: JenisKelamin;
  constructor(private service: SysadminService) {
    this.namaJenisKelaminMetaData = new InputTextItem("Jenis Kelamin", "Isi dengan Jenis Kelamin", "string");
    this.message = new MessageType();
    this.search = new Criteria();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.service.getJenisKelamin(this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.saveJenisKelamin(this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new JenisKelamin();
    this.message = new MessageType();
  }
  showDetail(item: JenisKelamin) {
    this.currentModel = item;
  }
  delete(item: JenisKelamin) {
    let self = this;
    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          self.service.deleteJenisKelamin(item).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }

}
