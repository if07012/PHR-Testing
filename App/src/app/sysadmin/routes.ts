import { AuthenticationResolver } from './../resolver/authentication.resolver';
// Imports
// Deprecated import
// import { RouterConfig } from '@angular/router';
import { Routes } from '@angular/router';


import { PageNotFoundComponent } from '../not-found.component';
import { SysAdminDashboardComponent } from "app/sysadmin/dashboard/dashboard.component";
import { UserComponent } from './user/user.component';
import { JenisKelamin } from '../models/jenis.kelamin';
import { JenisKelaminComponent } from './jenis-kelamin/jenis-kelamin.component';
import { AgamaComponent } from './agama/agama.component';
import { KotaComponent } from './kota/kota.component';
import { KecamatanComponent } from './kecamatan/kecamatan.component';
import { KelurahanComponent } from './kelurahan/kelurahan.component';
import { JenisPelayananComponent } from './jenis-pelayanan/jenis-pelayanan.component';
import { DepartemenComponent } from './departemen/departemen.component';
import { RuanganComponent } from './ruangan/ruangan.component';
import { SysadminDiagnosaComponent } from './diagnosa/diagnosa.component';
import { DokterComponent } from './dokter/dokter.component';
import { SysadminJadwalDokterComponent } from './jadwal-dokter/jadwal-dokter.component';


// Route Configuration
export const sysAdminRoutes: Routes = [
  {
    path: 'sysadmin', component: SysAdminDashboardComponent, resolve: {
      transaction: AuthenticationResolver
    }, children: [
      {
        path: 'jadwal-dokter', component: SysadminJadwalDokterComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'dokter', component: DokterComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'diagnosa', component: SysadminDiagnosaComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'ruangan', component: RuanganComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'departemen', component: DepartemenComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'jenis-pelayanan', component: JenisPelayananComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'kelurahan', component: KelurahanComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'kecamatan', component: KecamatanComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'kota', component: KotaComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }, {
        path: 'agama', component: AgamaComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'user', component: UserComponent, resolve: {
          transaction: AuthenticationResolver
        }
      },
      {
        path: 'jenis-kelamin', component: JenisKelaminComponent, resolve: {
          transaction: AuthenticationResolver
        }
      }


    ]
  },
];