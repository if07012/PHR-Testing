import { Component, OnInit } from '@angular/core';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { MessageType } from 'app/models/messageType';
import { Criteria } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { Paged } from 'app/models/paged';
import { Dokter } from "app/models/dokter";
import { SysadminService } from 'app/services/sysadmin.service';
declare var $: any;
@Component({
  selector: 'phr-dokter',
  templateUrl: './dokter.component.html',
  styleUrls: ['./dokter.component.css']
})
export class DokterComponent implements OnInit {


  namaPegawaiMetaData: InputTextItem;
  usernameMetaData: InputTextItem;
  passwordMetaData: InputTextItem;
  message: MessageType;
  search: Criteria;
  pages: Array<Paged>;
  list: Array<Dokter> = new Array<Dokter>();
  currentModel: Dokter;
  constructor(private service: SysadminService) {
    this.namaPegawaiMetaData = new InputTextItem("Dokter", "Isi dengan Nama Dokter", "string");
    this.usernameMetaData = new InputTextItem("User Id", "Isi dengan User Id", "string");
    this.passwordMetaData = new InputTextItem("Password", "Isi dengan Password", "password");
    this.message = new MessageType();
    this.search = new Criteria();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.service.getDokter(this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.saveDokter(this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new Dokter();
    this.message = new MessageType();
  }
  showDetail(item: Dokter) {
    this.currentModel = item;
  }
  delete(item: Dokter) {
    let self = this;
    this.currentModel = item;

    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          this.message = new MessageType();
          this.message.type = "loading";
          this.message.content = "Sedang proses....";
          self.service.delete<Dokter, string>("Dokter", item.id).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }


}
