import { Component, OnInit, AfterViewInit } from '@angular/core';
declare var $: any;
@Component({
  selector: 'phr-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class SysAdminDashboardComponent implements OnInit, AfterViewInit {
  ngAfterViewInit(): void {
    $("#menu").click(function () {
      $('.ui.sidebar')
        .sidebar('toggle')
    })
  }

  constructor() { }

  ngOnInit() {
  }

}
