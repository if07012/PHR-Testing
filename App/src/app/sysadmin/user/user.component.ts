import { Component, OnInit } from '@angular/core';
import { InputTextItem } from 'app/components/input-text/models/input.text.item';
import { InputTextComponent } from '../../components/input-text/input-text.component';
import { MessageType } from 'app/models/messageType';
import { UserModel } from "app/models/agama";
import { SysadminService } from "app/services/sysadmin.service";
import { Criteria } from 'app/home/pasien/daftar-registrasi/model/daftar.registrasi-filter';
import { environment } from 'environments/environment';
import { Paged } from 'app/models/paged';
declare var $: any;
@Component({
  selector: 'phr-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  userNameMetaData: InputTextItem;
  passwordMetaData: InputTextItem;
  confirmPasswordMetaData: InputTextItem;
  jenisUserMetaData: InputTextItem;
  message: MessageType;
  search: Criteria;
  pages: Array<Paged>;
  list: Array<UserModel> = new Array<UserModel>();
  currentModel: UserModel;
  constructor(private service: SysadminService) {
    this.userNameMetaData = new InputTextItem("User Name", "Isi dengan Username", "string");
    this.userNameMetaData.required = true;
    this.jenisUserMetaData = new InputTextItem("Jenis user", "Pilihlah jenis user", "select");
    this.jenisUserMetaData.required = true;
    this.jenisUserMetaData.data = {
      url: environment.SYSADMIN_ENDPOINT + "JenisUser/Search",
      fields: { title: 'namaJenisUser', description: 'Jenis User' },
      searchFullText: true
    };

    this.passwordMetaData = new InputTextItem("Password", "Isi dengan Password", "password");
    this.passwordMetaData.required = true;
    this.confirmPasswordMetaData = new InputTextItem("Confirm Password", "Isi dengan Confirm Password", "password");
    this.confirmPasswordMetaData.required = true;
    this.message = new MessageType();
    this.search = new Criteria();
  }

  changePage(page: Paged) {
    this.search.page = page.page;
    this.findData();
  }
  changeTake(take: number) {
    this.search.take = take;
    this.findData();
  }
  ngOnInit() {
    this.cancle();
    this.findData();
  }
  findData() {
    this.message.type = "loading";
    this.message.content = "Sedang proses....";
    this.service.getUser(this.search).subscribe(result => {
      this.list = result.records;
      this.pages = new Array<Paged>();
      let start = result.currentPage;
      this.search.take = result.take;
      if (start <= 5)
        start = 1;
      else if (start > 5)
        start = result.currentPage - 5;
      for (let i = start; i <= start + 9; i++) {
        if ((i - 1) * result.take < result.totalRecords)
          this.pages.push(new Paged(i, i == result.currentPage));
      }
      this.message = new MessageType();
    });
  }
  saveData() {

    let isEdit = this.currentModel.id !== undefined;
    if ((this.currentModel.Valid !== undefined && this.currentModel.Valid()) || this.currentModel.Valid === undefined) {
      this.message.type = "loading";
      this.message.content = "Sedang proses....";
      this.service.saveUser(this.currentModel).subscribe(result => {
        if (isEdit == false)
          this.list.unshift(result);
        else {
          this.findData();
        }
        this.cancle();
        this.message = new MessageType();
      });
    } else {
      this.message = new MessageType();
      this.message.content = this.currentModel.message;
      this.message.type = "404";
    }
  }
  cancle() {
    this.currentModel = new UserModel();
    this.message = new MessageType();
  }
  showDetail(item: UserModel) {
    this.currentModel = item;
    this.currentModel.confirmPassword = item.password;
  }
  delete(item: UserModel) {
    let self = this;
    $('.ui.modal')
      .modal({
        closable: false,
        onDeny: function () {
          return true;
        },
        onApprove: function () {
          self.service.deleteUser(item).subscribe(result => {
            self.findData();
            self.cancle();
          });
        }
      })
      .modal('show');

  }
}
