SET URL_ACCOUNT=http://localhost:7000
SET URL_SYSADMIN=http://localhost:7001/
SET URL_PATIENT=http://localhost:7002/
SET ASPNETCORE_ENVIRONMENT=Production
SET CONNECTION_STRING=User ID=sa;password=pass@w0rd1;server=.\sqlexpress;Database=phr_testing;MultipleActiveResultSets=true
SET PROVIDER=SQLSERVER
SET SERVER_RABBIT_MQ_ECG=localhost
SET SERVER_RABBIT_MQ=localhost
SET REST_URL_SYSADMIN=http://localhost:7001/
SET NO_TIMEOUT=true

CD /D Messaging
call cmd /C "npm start"