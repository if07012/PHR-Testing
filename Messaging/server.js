var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var amqp = require('amqplib/callback_api');
var list = [];
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});
var count = 0;


function Reload() {
    try {
        var url = process.env.SERVER_RABBIT_MQ;
        if (url === undefined)
            url = "localhost";
        amqp.connect('amqp://' + url, function (err, conn) {
            try {
                console.log('TryToConnect to '+url);
                conn.createChannel(function (err, ch) {
                    try {
                        console.log('Connected');
                        var ex = 'publishGenerateNumber';

                        ch.assertExchange(ex, 'fanout', {
                            durable: false
                        });

                        ch.assertQueue('', {
                            exclusive: true
                        }, function (err, q) {
                            try {
                                ch.bindQueue(q.queue, ex, '');
                                ch.consume(q.queue, function (msg) {
                                    console.log('receive');
                                    console.log(msg.content.toString());
                                    var obj = JSON.parse(msg.content.toString());
                                    console.log(obj.Id);
                                    io.emit(obj.Id, obj);
                                }, {
                                    noAck: true
                                });
                            } catch (e) {
                                console.log(e);
                            }
                        });
                    } catch (e) {
                        console.log(e);
                    }
                });
                conn.createChannel(function (err, ch) {
                    try {
                        var ex = 'BroadcastMessage';

                        ch.assertExchange(ex, 'fanout', {
                            durable: false
                        });

                        ch.assertQueue('', {
                            exclusive: true
                        }, function (err, q) {
                            try {
                                ch.bindQueue(q.queue, ex, '');
                                ch.consume(q.queue, function (msg) {
                                    console.log('receive BroadcastMessage');
                                    console.log(msg.content.toString());
                                    var obj = JSON.parse(msg.content.toString());
                                    io.emit(obj.To, obj.Content);
                                }, {
                                    noAck: true
                                });
                            } catch (e) {
                                console.log(e);
                            }
                        });
                    } catch (e) {
                        console.log(e);
                    }
                });
                conn.createChannel(function (err, ch) {
                    try {
                        var ex = 'BroadcastXmlRecive';
                        console.log('prepare BroadcastXmlRecive');
                        ch.assertExchange(ex, 'fanout', {
                            durable: false
                        });

                        ch.assertQueue('', {
                            exclusive: true
                        }, function (err, q) {
                            try {
                                console.log(err);
                                ch.bindQueue(q.queue, ex, '');
                                ch.consume(q.queue, function (msg) {
                                    console.log('receive xml');
                                    console.log(msg.content.toString());
                                    var obj = JSON.parse(msg.content.toString());
                                    console.log(obj.Id);
                                    io.emit(obj.Id, obj);
                                }, {
                                    noAck: true
                                });
                            } catch (e) {
                                setTimeout(function () {
                                    Reload();
                                }, 3000);
                            }
                        });
                    } catch (e) {
                        setTimeout(function () {
                            Reload();
                        }, 3000);
                    }
                });
            } catch (ex) {
                setTimeout(function () {
                    Reload();
                }, 3000);
            }
        });
    } catch (e) {
        setTimeout(function () {
            Reload();
        }, 3000);
    }

}

Reload();

try {
    SocketIO();
} catch (e) {
    setTimeout(function () {
        SocketIO();
    }, 3000);
}

function SocketIO() {
    io.on('connection', function (socket) {
        list.push(socket);

        socket.on('publish', function (msg) {
            io.emit(msg.room, msg);
        });

        socket.on('disconnect', function (msg) {
            console.log('user disconnect');
        });
        console.log('a user connected');
    });
}

http.listen(7008, function () {
    console.log('listening on *:7008');
});